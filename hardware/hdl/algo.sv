module algo (
input logic clk,
input logic rst,
input logic [31:0] curr_price,
input logic [31:0] daily_moving_average,
input logic [31:0] moving_avg_100,
input logic [31:0] moving_avg_1000,
output logic algo_done,
output logic buy,
output logic sell
);

//THIS IS YOUR ALGORITHM!
//if purely combinational valid can stay high the entire time;
//if is sequential, keep valid LOW until the algo is done, and then set HIGH for 1 cycle
//outputs are buy signal and sell signal


//diego algo #2:
//Description:
// input: 32 bit moving average for 100 point
// input: 32 bit moving average for 1000 point

assign algo_done = 1'b1;

logic crossed_dma;
logic crossed_100;
logic crossed_1000;

logic above_dma;
logic above_100;
logic above_1000;

logic above_all;
logic below_all;

always_comb begin

if(rst) begin
    above_dma = 1'b0;
    above_100 = 1'b0;
    above_1000 = 1'b0;
end 
else begin
    above_dma = curr_price > daily_moving_average;
    above_100 = curr_price > moving_avg_100;
    above_1000 = curr_price > moving_avg_1000;

    above_all = above_dma & above_100 & above_1000;
    below_all = ~above_dma & ~above_100 & above_1000;

end

end



always_ff @ (posedge clk) begin

if(rst) begin
    crossed_100 <= 1'b0;
    crossed_dma <= 1'b0;
    crossed_1000 <= 1'b0;
end
else begin

    if(above_all)
        buy <= ~buy;
    

end


end










endmodule