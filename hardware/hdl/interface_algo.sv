//`define UDP_SIZE 16384

module interface_algo
(
    input logic clk,
    input logic rst,
    input logic [16384 - 1:0] packet_in,
    input logic valid_in,
    output logic is_processing,
    output logic [1:0] trade,
    output logic ready_for_next_packet

);


//SM to algo connections
logic [63:0] curr_price;
logic price_upd_buy;
logic price_upd_sell;


//algo to SM connections
logic [63:0] target_security;


//axi interface between algo and SM
logic sm_valid;
logic algo_ready;

logic algo_valid;
logic sm_ready;

enum logic [1:0] {
    idle,
    buy,
    sell
} decision_enum;



assign trade = decision_enum;




sm sm(
    .clk(clk),
    .rst(rst),
    .valid(valid_in), 
    .d_in(packet_in),
    .target_security(target_security),
    .algo_ready(algo_ready),
    .sm_valid(sm_valid),
    .algo_valid(algo_valid),
    .curr_price_to_algo(curr_price),
    .price_upd_buy(price_upd_buy),
    .price_upd_sell(price_upd_sell),
    .ready_for_next_packet(ready_for_next_packet),
    .is_processing(is_processing)
);


dma_algo dma_algo(
    .clk(clk),
    .rst(rst),
    .sm_valid(sm_valid),
    .algo_ready(algo_ready),
    .sm_ready(1'b1),
    .algo_valid(algo_valid),
    .cur_price(curr_price),
    .decision(decision_enum),
    .target_security(target_security)
);





endmodule
