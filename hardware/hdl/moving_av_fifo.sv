module moving_av_fifo
#(parameter size = 100)
(
input logic rst,
input logic [31:0] d_in,
input logic enable,
output logic [31:0] moving_average
);


logic [31:0] shift_regs [0:size - 1];

logic [63:0] total;


always_ff @ (posedge enable or posedge rst) begin
    if(rst) begin
        for(int i = 0; i < size; i++) begin
            shift_regs[i] <= 32'b0;
        end
       
end
    else if(enable) begin
        for(int i = (size - 1); i > 0; i--) begin
            shift_regs[i] <= shift_regs[i - 1]; 
        end 
        shift_regs[0] <= d_in;
        
    end
end


always_comb begin
	
    if(rst) begin
		total = 64'b0; 
    end
    else begin
	total = 64'b0;
        for(int i = 0; i < size; i++) begin
            total = total + shift_regs[i];
        end
        moving_average = total / size;
    end

end










endmodule
