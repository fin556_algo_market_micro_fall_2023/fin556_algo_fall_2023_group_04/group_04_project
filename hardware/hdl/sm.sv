module sm(
    input logic clk,
    input logic rst,
    input logic valid,
    input logic [16383:0] d_in,
    input logic [63:0] target_security,
    input logic algo_ready,
    output logic sm_valid,
    input logic algo_valid,
    output logic [63:0] curr_price_to_algo,
    output logic price_upd_buy,
    output logic price_upd_sell,
    //output logic load_array,
    //output logic [14:0] addr
    output logic ready_for_next_packet,
    output logic buy,
    output logic sell,
    output logic is_processing

);

//logic [5:0] curr_state, next_state;

int num_messages;

reg [31:0] message_count;
int curr_packet_size;

logic [14:0] curr_packet_location;
logic [7:0] curr_protocol;
logic [63:0] curr_security;


logic [63:0] curr_price;
logic [31:0] ieee_price;


logic analysis_done;

logic [31:0] daily_moving_average;
logic [31:0] daily_count; 
logic [31:0] daily_total;

logic enable_fifo;

logic [31:0] moving_average_100_out;
logic [31:0] moving_average_1000_out;

logic [31:0] moving_average_100;
logic [31:0] moving_average_1000;

logic moving_averages_ready;

//FF array values
logic [7:0] flip_flop_array [0:2047];
logic load_array;
logic [14:0] addr;

enum int unsigned {
    STARTUP,
    IDLE,
    PARSE_ARRAY,
    PACKET_INFO,
    PACKET_UPDATE,
    PACKET_ANALYSIS,
    ALGO_RUNNING
} curr_state, next_state;

// parameter STARTUP = 6'b111111;
// parameter IDLE = 6'b00000;
// parameter PARSE_ARRAY = 6'b000001;
// parameter PACKET_INFO = 6'b000010;
// parameter PACKET_UPDATE = 6'b000011;
// parameter PACKET_ANALYSIS = 6'b011100;
// parameter ALGO_RUNNING = 6'b000101;

always_ff @ (posedge clk) begin
    if(rst)
        curr_state <= STARTUP;
    else
        curr_state <= next_state;
end

function void set_defaults();
    load_array = 1'b0;
    addr = 14'b0;

endfunction

always_ff @ (posedge clk) begin
    case(curr_state)
STARTUP: begin
    message_count <= 1'b0;
end

IDLE: begin
    message_count <= 1'b0;
end

PARSE_ARRAY: begin
    if(num_messages > 0)
        message_count <= num_messages;
end

PACKET_UPDATE: begin
    message_count <= message_count - 32'h00000001;
end

    endcase

end


always_comb begin
    is_processing = 1'b1;
    case(curr_state)


    STARTUP: begin
	is_processing = 1'b0;
        num_messages = 0;
        curr_packet_size = 0;
        //message_count = 0;
        curr_packet_location = 15'h52;
        ready_for_next_packet = 1'b0;
        daily_count = 0;
        daily_moving_average = 0;
        daily_total = 0;
        next_state = IDLE; //maybe wait a few cycles
        enable_fifo = 1'b0;
        moving_averages_ready = 1'b0;
        sm_valid = 1'b0;
        price_upd_buy = 1'b0;
        price_upd_sell = 1'b0;

    end

    IDLE: begin
	is_processing = 1'b0;
        num_messages = 0;
        curr_packet_size = 0;
        //message_count = 0;
        curr_packet_location = 15'h52;
        ready_for_next_packet = 1'b1;
        if(valid) begin
            next_state = PARSE_ARRAY;
            load_array = 1'b1;
        end
        else
            next_state = IDLE;
    end

    PARSE_ARRAY: begin
        load_array = 1'b0;
        ready_for_next_packet = 1'b0;
        //message count at byte x38
        num_messages = flip_flop_array[15'h38];
        //start at first message order (after header (start on byte 42))
        //look at what kind of order message it is
        if(num_messages > 0) begin
            //message_count = num_messages;
            next_state = PACKET_INFO;
        end
        else begin
            next_state = IDLE;
        end
        //next_state = IDLE;
    end

    PACKET_INFO: begin
        curr_packet_size = flip_flop_array[curr_packet_location]; //assume size not greater than xFF
        curr_protocol = flip_flop_array[curr_packet_location + 2];
        curr_price = 64'b0;
        enable_fifo = 1'b0;

        //curr_price = 64'({<<8{{flip_flop_array[curr_packet_location + 24], flip_flop_array[curr_packet_location + 25], flip_flop_array[curr_packet_location + 26], flip_flop_array[curr_packet_location + 27], flip_flop_array[curr_packet_location + 28], flip_flop_array[curr_packet_location + 29], flip_flop_array[curr_packet_location + 30], flip_flop_array[curr_packet_location + 31]}}});
        if(curr_protocol == 8'h38 || curr_protocol == 8'h35) begin//short sale right now to test - PLU is in same location
            curr_security = {flip_flop_array[curr_packet_location + 12], flip_flop_array[curr_packet_location + 13], flip_flop_array[curr_packet_location + 14], flip_flop_array[curr_packet_location + 15], flip_flop_array[curr_packet_location + 16], flip_flop_array[curr_packet_location + 17], flip_flop_array[curr_packet_location + 18], flip_flop_array[curr_packet_location + 19]};
            curr_price =  64'({<<8{{flip_flop_array[curr_packet_location + 24], flip_flop_array[curr_packet_location + 25], flip_flop_array[curr_packet_location + 26], flip_flop_array[curr_packet_location + 27], flip_flop_array[curr_packet_location + 28], flip_flop_array[curr_packet_location + 29], flip_flop_array[curr_packet_location + 30], flip_flop_array[curr_packet_location + 31]}}});
        end
        else
            curr_security = 32'h0;

        if(((curr_protocol == 8'h35 || curr_protocol == 8'h38) && curr_security == target_security)) begin //SPY HEX - 53 50 59 20  20 20 20 20
            next_state = PACKET_ANALYSIS;
        end
        else begin
            next_state = PACKET_UPDATE;
        end

    end

    PACKET_UPDATE: begin //go here after a miss or analyze
        curr_protocol = 8'b0;
        curr_security = 64'b0;
        curr_price = 64'b0;
        //message_count = message_count - 32'h00000001;
        curr_packet_location = curr_packet_location + curr_packet_size + 2;
        if(message_count != 1) //hardcode fix for massive
            next_state = PACKET_INFO;
        else
            next_state = IDLE;
    end



    PACKET_ANALYSIS: begin


        
        //if((curr_protocol == 8'h35 || curr_protocol == 8'h38)) begin
        //curr_security = {flip_flop_array[curr_packet_location + 12], flip_flop_array[curr_packet_location + 13], flip_flop_array[curr_packet_location + 14], flip_flop_array[curr_packet_location + 15], flip_flop_array[curr_packet_location + 16], flip_flop_array[curr_packet_location + 17], flip_flop_array[curr_packet_location + 18], flip_flop_array[curr_packet_location + 19]};
        //     curr_price =  64'({<<8{{flip_flop_array[curr_packet_location + 24], flip_flop_array[curr_packet_location + 25], flip_flop_array[curr_packet_location + 26], flip_flop_array[curr_packet_location + 27], flip_flop_array[curr_packet_location + 28], flip_flop_array[curr_packet_location + 29], flip_flop_array[curr_packet_location + 30], flip_flop_array[curr_packet_location + 31]}}});
        //end

        // modulo_price <= curr_price % 100;

        if(algo_ready) begin
            sm_valid = 1'b1;
            curr_price_to_algo = curr_price;
            price_upd_buy = curr_protocol == 8'h38;
            price_upd_sell = curr_protocol == 8'h35;
        end
        
        // daily_count = daily_count + 1;

        // if(daily_count > 1000)
        //     moving_averages_ready = 1'b1;

        // daily_total = daily_total + (curr_price / 100);

        // enable_fifo = 1'b1;


        // daily_moving_average = daily_total / daily_count;
        
        // if(moving_averages_ready) begin
        //     moving_average_100 = moving_average_100_out;
        //     moving_average_1000 = moving_average_1000_out;
        // end
        // else begin
        //     moving_average_100 = 32'b0;
        //     moving_average_1000 = 32'b0;
        // end
        

        //analysis_done = 1'b1;

        next_state = ALGO_RUNNING;
        
    end


    ALGO_RUNNING: begin

    //can be replaced with stronger algo

    
    price_upd_buy = 1'b0;
    price_upd_sell = 1'b0;
    sm_valid = 1'b0;
    enable_fifo = 1'b0;
    //analysis_done = 1'b1; //hardcode rn
    



    if(algo_valid)
        next_state = PACKET_UPDATE;

    end




    endcase


end


// algo algo(
// .clk(clk),
// .rst(rst),
// .curr_price(curr_price),
// .daily_moving_average(daily_moving_average),
// .moving_avg_100(moving_average_100),
// .moving_avg_1000(moving_average_1000),
// .algo_done(analysis_done),
// .buy(buy),
// .sell(sell)
// );







always_ff @(posedge load_array) begin
        for (int i = 0; i < 2048; i++) begin
            // Construct 16-bit chunks using concatenation
            flip_flop_array[i] <= {d_in[(i + 1) * 8 - 1], d_in[(i + 1) * 8 - 2], d_in[(i + 1) * 8 - 3], d_in[(i + 1) * 8 - 4], d_in[(i + 1) * 8 - 5], d_in[(i + 1) * 8 - 6], d_in[(i + 1) * 8 - 7], d_in[i * 8]};
        end
    end



// _64_to_ieee _64_to_ieee(
//     .d_in(curr_price),
//     .d_out(ieee_price)
// // );

// moving_av_fifo #(.size(100))
// moving_av_fifo_100
// (
// .clk(clk),
// .rst(rst),
// .d_in(curr_price),
// .enable(enable_fifo),
// .moving_average(moving_average_100_out)


// );


// moving_av_fifo #(.size(1000))
// moving_av_fifo_1000
// (
// .clk(clk),
// .rst(rst),
// .d_in(curr_price),
// .enable(enable_fifo),
// .moving_average(moving_average_1000_out)


// );








endmodule
