module dma_algo (
    input logic clk, rst,
    input logic sm_valid,
    output logic algo_ready,
    input logic sm_ready,
    output logic algo_valid,
    input logic [63:0] cur_price,
    output logic [1:0] decision,
    output logic [63:0] target_security
);

enum int unsigned {
    under_init,
    under_const,
    above_init,
    above_const
} state, next_state;

enum logic [1:0] {
    idle,
    buy,
    sell
} decision_enum;

logic [63:0] dma, ma100, ma1000, ma100_beforecount, ma1000_beforecount;

logic above_dma, above_ma100, above_ma1000;
logic ma_valid;

logic [63:0] count;
logic [63:0] total_price;

assign decision = decision_enum;

assign target_security = 64'h5350592020202020;

assign dma = total_price / count;

assign algo_ready = 1'b1;
assign algo_valid  =1'b1;

assign above_dma = cur_price > dma;
assign above_ma100 = cur_price > ma100;
assign above_ma1000 = cur_price > ma1000;


assign ma100 = count > 100 ? ma100_beforecount : 64'bX;
assign ma1000 = count > 1000 ? ma1000_beforecount : 64'bX;
assign ma_valid = count > 1000 ? 1'b1 : 1'b0;

always_comb begin : state_outputs
    unique case (state)
        under_const, above_const: decision_enum = idle;
        under_init: decision_enum = ma_valid ? buy : idle;
        above_init: decision_enum = ma_valid ? sell : idle;
    endcase
end : state_outputs

always_comb begin : next_state_logic
    unique case (state)
        under_const: next_state = above_dma & above_ma100 & above_ma1000 ? 
                        above_init : under_const;
        above_const: next_state = ~above_dma & ~above_ma100 & ~above_ma1000 ? 
                        under_init : above_const;
        under_init: next_state = under_const;
        above_init: next_state = above_const;
    endcase
end : next_state_logic

always_ff @(posedge clk) begin
    if (rst) state <= above_const;
    else state <= next_state;
end

always_ff @ (posedge sm_valid or rst) begin
    if(rst) begin
        count <= 0;
        total_price <= 0;
    end
    else if(sm_valid) begin
        count <= count + 1;
        total_price <= total_price + cur_price;
    end
end


moving_av_fifo #(.size(100))
moving_av_fifo_100
(
.rst(rst),
.d_in(cur_price),
.enable(sm_valid),
.moving_average(ma100_beforecount)


);


moving_av_fifo #(.size(1000))
moving_av_fifo_1000
(
.rst(rst),
.d_in(cur_price),
.enable(sm_valid),
.moving_average(ma1000_beforecount)


);




endmodule : dma_algo
