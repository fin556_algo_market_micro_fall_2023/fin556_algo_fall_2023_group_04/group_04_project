module conv_price_fp32 (    
    input logic [63:0] din,
    output logic [31:0] dout
);
    logic [63:0] integral_din;
    logic [31:0] fractional_din;
    logic [31:0] fractional_dout;
    
    assign integral_din = din / 10000;
    assign fractional_din = din % 10000;

    int norm_amt, frac_idx;
    int cum_mul, mant_idx;
    logic [95:0] mantissa_concat;
    logic [22:0] mantissa;
    logic [7:0] exponent;

    always_comb begin : fractional_calc
        norm_amt = 0;
        cum_mul = fractional_din;
        fractional_dout = 32'b0;
        for (int i = 31; i >= 0; i--) begin
            norm_amt++;
            cum_mul *= 2;
            fractional_dout[i] = cum_mul >= 10000 ? 1'b1 : 1'b0;
            cum_mul = cum_mul >= 10000 ? cum_mul - 10000 : cum_mul;
            if (cum_mul == 0)
                break;
        end
        frac_idx = 0;
        for ( int i = 31; i >= 0; i--) begin
            frac_idx++;
            if(fractional_dout[i] == 1'b1)
                break;
        end

        norm_amt = frac_idx;
        frac_idx = 0;
        for (int i = 0; i < 64; i++) begin
            if (integral_din[i])
                frac_idx = i;
        end

        norm_amt = frac_idx == 0 ? norm_amt : frac_idx;
        mantissa_concat = {integral_din, fractional_dout};
    
        mant_idx = 0;
        for (int i = 95; i >= 0; i--) begin
            if (mantissa_concat[i] == 1'b1) begin
                mant_idx = i;
                break;
            end
        end

        mantissa = mantissa_concat[(mant_idx-1)-:23];
        if(norm_amt == frac_idx)
            exponent = 8'(127 + norm_amt);
        else
            exponent = 8'(127 - norm_amt);

        dout = din == 0 ? 32'b0 : {1'b0, exponent, mantissa};
    end : fractional_calc
endmodule : conv_price_fp32
