module ff_array(
    input logic [16383:0] d_in, // 16384-bit input
    input logic load, // Load signal to trigger parallel loading
    output logic [7:0] q_out, // Output from the addressed flip-flop
    input logic [14:0] address // 15-bit address for 16384/8 = 2048 address locations
);

    // Create an array of 1024 flip-flops to store 16 bits each
    logic [7:0] flip_flop_array [0:2047];

    // Parallel loading of 16-bit chunks of d_in into flip-flop array
    always_ff @(posedge load) begin
        for (int i = 0; i < 2048; i++) begin
            // Construct 16-bit chunks using concatenation
            flip_flop_array[i] <= {d_in[(i + 1) * 8 - 1], d_in[(i + 1) * 8 - 2], d_in[(i + 1) * 8 - 3], d_in[(i + 1) * 8 - 4], d_in[(i + 1) * 8 - 5], d_in[(i + 1) * 8 - 6], d_in[(i + 1) * 8 - 7], d_in[i * 8]};
        end
    end

    // Read operation: Retrieve data from the specified address location
    assign q_out = (address < 2048) ? flip_flop_array[address] : 16'hXXXX; // Return XXXX if address is out of range

endmodule