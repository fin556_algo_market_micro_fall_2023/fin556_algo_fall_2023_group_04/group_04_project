`timescale 1ns / 1ps

typedef struct packed {
    logic [31:0] magic_number;
    logic [15:0] major_version;
    logic [15:0] minor_version;
    logic [63:0] reserved;
    logic [31:0] snaplen;
    logic [2:0] frame_check_sequence;
    logic f;
    logic [11:0] zeroes;
    logic [15:0] link_type;
} pcap_file_header;

typedef struct packed {
    logic [31:0] timestamp_s;
    logic [31:0] timestamp_ns;
    logic [31:0] capture_length;
    logic [31:0] original_length;
} pcap_packet_header;

module tb_harness();

parameter START_PACKET_IDX = 0;
parameter END_PACKET_IDX = 250000;
parameter TIME_SKIP = 1;
parameter SIM_LENGTH_NS = 10000000;
parameter PROGRESS_REPORT = 1;
parameter PROGRESS_DENOMINATION = 5000;

//nanosecond clock
bit clk;
//microsecond clock
bit micro_clk;

initial clk = 1'b1;
initial micro_clk = 1'b1;

always #0.5 clk = ~clk;
always #500 micro_clk = ~micro_clk;

// Harness Vars
longint timeout = SIM_LENGTH_NS;

logic rst, read_next;
pcap_file_header file_hdr;
pcap_packet_header pkt_hdr;
logic [31:0] packet_length;
logic [16383:0] curr_packet, next_packet;

int pcap_fd, log_fd;
int next_packet_idx;
longint curr_ts, next_ts;
logic [31:0] fp_price;

// Algo Vars
logic [16383:0] algo_packet;
logic packet_valid;
logic algo_ready_for_next_packet;
logic [1:0] trade;
logic is_processing;

logic jb_staller;
logic jb_staller_delay;

interface_algo interfacealgo(
    .clk(clk),
    .rst(rst),
    .packet_in(algo_packet),
    .valid_in(packet_valid),
    .trade(trade),
    .ready_for_next_packet(algo_ready_for_next_packet),
    .is_processing(is_processing)
);

conv_price_fp32 fp32_conv (
    .din(interfacealgo.sm.curr_price),
    .dout(fp_price)
);

task read_next_packet();
    next_packet = '0;
    next_packet_idx++;
    $fread(pkt_hdr, pcap_fd);
    packet_length = int'({<<8{pkt_hdr.capture_length}});
    for (int i = 0; i < packet_length; i++) begin
        $fread(next_packet[(i*8)+:8], pcap_fd);
    end
    next_ts = next_packet[655:592];
endtask : read_next_packet

task do_rst ();
    rst <= 1'b1;
    packet_valid <= 1'b0;
    read_next <= 1'b0;
    pcap_fd = $fopen("../pcap-memdump/big_spy_only.pcap", "r");
    log_fd = $fopen("../log.csv", "w");
    $fread(file_hdr, pcap_fd);
    for (int i = 0; i < START_PACKET_IDX + 1; i++) begin : start_offset
        read_next_packet();
    end : start_offset
    next_packet_idx <= START_PACKET_IDX;
    @(posedge clk);
    rst <= 1'b0;
endtask

always_ff @(posedge clk) begin : realtime_clock
    curr_ts <= rst || (TIME_SKIP > 0 && jb_staller) ? next_ts - 10 : curr_ts + 1;
end : realtime_clock

always_ff @ (posedge clk) begin : rising_edge_detector
    jb_staller_delay <= algo_ready_for_next_packet;
end : rising_edge_detector

assign jb_staller = ~jb_staller_delay & algo_ready_for_next_packet;

always_comb begin
    if(algo_ready_for_next_packet) begin
        algo_packet = curr_packet;
    end
    else begin
        algo_packet = 16384'b0;
    end
end

always_ff @(posedge clk) begin : data_logging
    if (trade == 2)
        $fdisplay(log_fd, "%0d,%32b,SELL", curr_ts, fp_price);
    if (trade == 1)
        $fdisplay(log_fd, "%0d,%32b,BUY", curr_ts, fp_price); 
end : data_logging

initial begin
    $fsdbDumpfile("dump.fsdb");
	$fsdbDumpvars(0, "+all");
    do_rst();
    @(posedge clk);
    while (!$feof(pcap_fd) && next_packet_idx < END_PACKET_IDX) begin : read_loop
        if ((curr_ts == next_ts)) begin
            if ((next_packet_idx % PROGRESS_DENOMINATION == 0) && PROGRESS_REPORT > 0)
                $display("Simulated up to packet %0d", next_packet_idx);
            curr_packet <= next_packet;
            packet_valid <= 1'b1;
            read_next_packet();
        end else
            packet_valid <= 1'b0;
        @(posedge clk);
    end : read_loop
    
    @(posedge clk & jb_staller);
    $finish;
end

always_ff @(posedge clk) begin : timeout_calc
    if (timeout == 0) $finish;
    timeout <= timeout - 1;
end : timeout_calc

endmodule : tb_harness
