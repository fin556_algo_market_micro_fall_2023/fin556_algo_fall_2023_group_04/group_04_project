`timescale 1ns / 1ps

module tb_1400();

parameter NUM_PACKETS = 1000;
parameter START_PACKET_IDX = 500;
parameter END_PACKET_IDX = 1000;
parameter TIME_SKIP = 1;
parameter SIM_LENGTH_NS = 1000000;

//nanosecond clock
bit clk;
//microsecond clock

initial clk = 1'b1;

always #0.5 clk = ~clk;


//harness vars
logic rst;
logic [16384-1:0] packet_in [NUM_PACKETS];
logic [63:0] timestamp, next_ts;

logic [31:0] next_packet_idx;
logic [16383:0] cur_packet;
logic packet_resp;

//algo vars
logic [16383:0] algo_packet;
logic packet_valid;
logic algo_ready_for_next_packet;
logic trade;
logic is_processing;

logic jb_staller;
logic jb_staller_delay;

logic [31:0] fp32_price;

conv_price_fp32 fp32_conv (
    .din(interfacealgo.sm.curr_price),
    //.din(64'd93124_6400),
    .dout(fp32_price)
);


task do_rst();
    rst <= 1'b1;
    @(posedge clk);
    rst <= 1'b0;
    $readmemh("../pcap-memdump/spy_only.mem", packet_in);
    @(posedge clk);
    $display("TIMESTAMP: %0d", timestamp);
    repeat (10) @(posedge clk);
endtask

always_ff @(posedge clk) begin
    if(rst)
        timestamp <= packet_in[START_PACKET_IDX][655:592] - 10;
    else
        if(jb_staller && TIME_SKIP > 0)
            timestamp <= next_ts - 10;
        else
            timestamp <= timestamp + 1;
end

always_ff @(posedge clk) begin
    if(rst) begin
        next_packet_idx = START_PACKET_IDX;
        next_ts = packet_in[START_PACKET_IDX][655:592];
        cur_packet = 'x;
        packet_valid = 1'b0;
    end else begin
        if(timestamp == next_ts) begin
            if (next_packet_idx % 10000 == 0)
                $display("Processed Packet %7d...", next_packet_idx);
            cur_packet <= packet_in[next_packet_idx];
            next_packet_idx <= next_packet_idx + 1;
            next_ts <= packet_in[next_packet_idx + 1][655:592];
            packet_valid <= 1'b1;
        end
        else
            packet_valid <= 1'b0;
    end
end

assign jb_staller = ~jb_staller_delay & algo_ready_for_next_packet;

always_ff @ (posedge clk) begin
    jb_staller_delay <= algo_ready_for_next_packet;
end

always_comb begin
    if(algo_ready_for_next_packet) begin
        algo_packet = cur_packet;
    end
    else begin
        algo_packet = 16384'b0;
    end
end

int proc_duration, fd;
always_ff @(posedge clk) begin
    if(rst)
        proc_duration = 0;
    else begin
        if (is_processing == 1'b1) begin
            proc_duration <= proc_duration + 1;
        end else begin
            if (proc_duration != 0) $fwrite(fd, "%0d,", proc_duration);
            proc_duration <= 0;
        end
    end
end

interface_algo interfacealgo(
    .clk(clk),
    .rst(rst),
    .packet_in(algo_packet),
    .valid_in(packet_valid),
    .trade(trade),
    .ready_for_next_packet(algo_ready_for_next_packet),
    .is_processing(is_processing)
);

always_ff @(posedge clk) begin
    if(((next_packet_idx > NUM_PACKETS-1) && jb_staller) || (next_packet_idx > END_PACKET_IDX-1)) begin
        $finish();
    end
end

initial begin
    $fsdbDumpfile("dump.fsdb");
	$fsdbDumpvars(0, "+all");
    fd = $fopen("proc_dur.csv", "w");

    do_rst();
    repeat (SIM_LENGTH_NS) @(posedge clk);
    //repeat (200000) @(posedge clk);
    
    $finish;

end


//packet nums
//30390 to 33200





endmodule
