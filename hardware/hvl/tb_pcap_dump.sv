`timescale 1ns / 1ps

module tb_pcap_dump();


//nanosecond clock
bit clk;
//microsecond clock

initial clk = 1'b1;

always #0.5 clk = ~clk;


//harness vars
logic rst;
logic [16384-1:0] packet_in [1400];
logic [63:0] timestamp, next_ts;

logic [31:0] next_packet_idx;
logic [16383:0] cur_packet;
logic packet_resp;

task do_rst();
    rst <= 1'b1;
    @(posedge clk);
    rst <= 1'b0;
    $readmemh("../pcap-memdump/packet.mem", packet_in);
    @(posedge clk);
    $display("TIMESTAMP: %0d", timestamp);
    repeat (10) @(posedge clk);
endtask

always_ff @(posedge clk) begin
    if(rst)
        timestamp <= packet_in[2][655:592] - 10;
    else
        timestamp <= timestamp + 1;
end

always_ff @(posedge clk) begin
    if(rst) begin
        next_packet_idx = 32'd5;
        next_ts = packet_in[2][655:592];
        cur_packet = 'x;
    end else begin
        if(timestamp == next_ts) begin
            cur_packet <= packet_in[next_packet_idx];
            next_packet_idx <= next_packet_idx + 1;
            next_ts <= packet_in[next_packet_idx + 1][655:592];
        end
    end
end

initial begin
    $fsdbDumpfile("dump.fsdb");
	$fsdbDumpvars(0, "+all");

    do_rst();
    repeat (20000000) @(posedge clk);

    $finish;

end






endmodule
