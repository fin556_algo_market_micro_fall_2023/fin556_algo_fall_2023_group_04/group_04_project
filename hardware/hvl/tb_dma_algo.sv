`timescale 1ns / 1ps

module tb_top();


//nanosecond clock
bit clk;
//microsecond clock
bit micro_clk;

initial clk = 1'b1;
initial micro_clk = 1'b1;

always #0.5 clk = ~clk;
always #500 micro_clk = ~micro_clk;

logic [31:0] dma, ma1c, ma1k, cp;
logic [1:0] decision;
logic rst;

dma_algo dma_algo_dut (
    .clk(clk),
    .rst(rst),
    .dma(dma),
    .ma100(ma1c),
    .ma1000(ma1k),
    .cur_price(cp),
    .decision(decision)   
);

task do_rst ();
    rst <= 1'b1;
    dma <= 32'd5;
    ma1c <= 32'd10;
    ma1k <= 32'd15;
    cp <= 32'd20;
    @(posedge clk);
    rst <= 1'b0;
    @(posedge clk);
endtask

initial begin
    $fsdbDumpfile("dump.fsdb");
	$fsdbDumpvars(0, "+all");
    do_rst();
    @(posedge clk);
    cp <= 32'd13; 
    @(posedge clk);
    cp <= 32'd9; 
    @(posedge clk);
    cp <= 32'd4; 
    @(posedge clk);
    cp <= 32'd7; 
    @(posedge clk);
    cp <= 32'd3; 
    @(posedge clk);
    cp <= 32'd11; 
    @(posedge clk);
    cp <= 32'd17; 
    @(posedge clk);
    cp <= 32'd12; 
    @(posedge clk);
    cp <= 32'd1; 
    repeat (5) @(posedge clk);
    $finish;
end

endmodule
