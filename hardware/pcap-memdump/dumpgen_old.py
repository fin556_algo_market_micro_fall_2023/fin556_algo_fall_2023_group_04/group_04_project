import json

with open("1400_packet_test.json") as rawFile:
    rawPackets = json.load(rawFile)
for num, packet in enumerate(rawPackets):
    r = f"{packet['_source']['layers']['frame_raw'][0]:<04096s}"
    print("".join(reversed([r[i:i+2] for i in range(0, len(r), 2)])))

