import ijson

INPUT_JSON = "spy_only.json"
OUTPUT_MEM = "spy_only.mem"

with open(OUTPUT_MEM, "w") as outfile:
    print("reading json...")
    with open(INPUT_JSON, "r") as rawFile:
        rawframes = ijson.items(rawFile, 'item')
        for num, frame in enumerate(rawframes):
            r = f"{frame['_source']['layers']['frame_raw'][0]:<04096s}"
            outfile.write("".join(reversed([r[i:i+2] for i in range(0, len(r), 2)])) + "\n")
            print(f"{num} packets written to memory...")
        
    # lenpackets = len(rawPackets)
    # for num, packet in enumerate(rawPackets):
    #     r = f"{packet['_source']['layers']['frame_raw'][0]:<04096s}"
    #     outfile.write("".join(reversed([r[i:i+2] for i in range(0, len(r), 2)])) + "\n")
    #     print(f"{num} out of {lenpackets} written...")
