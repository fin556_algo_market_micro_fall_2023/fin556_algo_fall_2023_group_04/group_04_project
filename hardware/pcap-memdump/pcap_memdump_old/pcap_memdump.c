#include "pcap_memdump.h"
#include <errno.h>
#include <limits.h>
#include <pcap.h>
#include <pcap/pcap.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv) {
  char *p;
  int limit;

  if (argc != 3) {
    printf("Invalid args");
    return -1;
  }
  errno = 0;
  long conv = strtol(argv[2], &p, 10);
  if (errno != 0 || *p != '\0' || conv > INT_MAX || conv < INT_MIN) {
    printf("Invalid args");
    return -1;
  }
  limit = conv;
  memdump(argv[1], limit);
}

int *hexify(const u_char *packet, int size, char *hexstr) {
  const char hexTable[] = "0123456789abcdef";
  u_int8_t upperNibble;
  u_int8_t lowerNibble;
  int i;

  for (i = 0; i < size * 2; i++) {
    upperNibble = packet[i / 2] >> 4;
    lowerNibble = packet[i / 2] & 0xF;
    hexstr[i] = i % 2 == 0 ? hexTable[upperNibble] : hexTable[lowerNibble];
  }
  return 0;
}

int memdump(char *filename, int limit) {
  char errbuf[PCAP_ERRBUF_SIZE];
  int i;
  const u_char *packet;
  struct pcap_pkthdr packet_header;

  pcap_t *handle = pcap_open_offline_with_tstamp_precision(
      filename, PCAP_TSTAMP_PRECISION_NANO, errbuf);

  if (handle == NULL) {
    printf("failed to obtain pcap handle | %s\n", errbuf);
    exit(-1);
  }

  pcap_activate(handle);

  printf("memdumping %d packets\n", limit);
  for (i = 0; i < limit; i++) {
    packet = pcap_next(handle, &packet_header);
    if (packet == NULL) {
      printf("error reading packet\n");
      break;
    }
    char packet_hex[packet_header.caplen * 2];
    hexify(packet, packet_header.caplen, packet_hex);
    printf("Read packet %d: %s\n\n", i, packet_hex);
  }

  pcap_close(handle);

  return 0;
}
