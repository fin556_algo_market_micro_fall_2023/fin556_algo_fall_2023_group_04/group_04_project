#ifndef PCAP_MEMDUMP_H
#define PCAP_MEMDUMP_H
#include <pcap/pcap.h>

int memdump(char *filename, int limit);

#endif
