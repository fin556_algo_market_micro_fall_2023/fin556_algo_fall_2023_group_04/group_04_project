# Group 4 Project Proposal

## Team Introductions

### Jason Romps
I am a Senior at UIUC studying Computer Engineering with a concentration in digital design and will be graduating in May 2024. I have lots of industry experience in FPGA & ASIC design and verification. I am the team lead for our low-latency hardware trading application. You can reach me at jasonar2@illinois.edu

### Diego Rapppaccioli
I am a Senior at UIUC studying Industrial Engineering with a minor in Computer Engineering and will be graduating in December. I have basic programming experience and have been able to work with large financial firms in the past. You can reach me at diegor3@illinois.edu. My Linkedin: is https://www.linkedin.com/in/diego-rappaccioli-gomez-10207b1b4

### Lijiajin Lin
I am a Senior at UIUC studying Computer Science concentrated in system design and will be graduating in May 2024. I have lots of  experience in distributed system design. You can reach me at llin17@illinois.edu

### Adrian Cheng
I am a Senior at UIUC studying Computer Engineering graduating in December 2023. I have industry experience in full stack development and I have experience in created automated deployments in a variety of environments. You can reach me at acheng27@illinois.edu.

## Our Strategy


Given that our project strives to provide hardware implementation of our strategies our trading strategy aims to optimize the hardware implementation without overextending to the point that may put the harwdare implementation out of scope. 

We aim to use the vasts amount of data on price movements and trade volume available on the IEX DEEP pcap files to build a momentum strategies on two extremely volume intensive equities. At the moment we are looking at the APPL and SPY as the tickets that we will be building our trading strategy on. 

Firstly we aim to parse the IEX DEEP data based on the parameters that we expect to influence our trading strategy which are price changes, historical price moving averages and trading volume. After we parse our data and identify how to constantly pull in this information we aim to train our ML model to take in moving averages (intra-day or week), rapid day movements and volume to indentify if there are are indicators of buy and sell signals during these spikes in liquidty and ruputure of intraday or week moving averages. After training our model we aim to backtest our strategy after implementing our algo's through hardware to detect the actual P&L effectivness of our model assumptions.

![Local Image](./strat_img.png)





## Hardware Implementation 


The hardware side of this project will be developed on in Verilog and SystemVerilog in Vivado 2022.2. Simulations will be done on SimVision and testbenches will be created to thoroughly test the designs. 

To start in simulations, a testing harness will be created that effectively models the UDP payload being transfered to our algorithm at a specific timestamp to model real packet transfer. The UDP payloads will be existing IEX DEEP data. The testing harness will also be able to assert any type of trade type at any specific time.

We will implement the DNN (Deep Neural Network) in hardware through implmenting systolic arrays to speed up all matrix multiplication operations, whilst pipelining each of the layers within the neural network so that order
throughput can be optimized. We have will implement a vagrant box complete with the Vivado suite which will serve as our development environment, as well as a link to the actual X3522 board. The neural network weights would
first be trained in software, then directly fed into the memory of the board for our network to use. Additionally, real IEX DEEP data will be formatted into raw binary which, through our serial testing harness, will replay
the historical orders aligned with the actual timings of the orders sent.


## Software Model Training 

In the evolving landscape of quantitative finance, sophisticated algorithms have become imperative for predicting stock market movements. Deep learning, a sub-field of machine learning, offers powerful tools to unearth patterns in time series data such as stock prices. Among them, the Long Short-Term Memory (LSTM) model, a variant of Recurrent Neural Networks (RNN), stands out for its capability to capture long-term temporal dependencies, making it particularly suited for stock market predictions.

### Technologies and Frameworks
For our project, we intend to harness the following tools and libraries:

Python: A versatile language known for its libraries suitable for data science and machine learning.
PyTorch: A prominent deep learning framework that offers flexibility in designing and training deep neural networks.
LSTM (Long Short-Term Memory): A powerful RNN variant that can remember patterns over long intervals. This makes it apt for time series prediction as it can recall distant past information, bridging the gap between past and present. Furthermore, LSTMs help in avoiding the vanishing gradient problem encountered in traditional RNNs.
IEX Cloud API: To reliably source financial data such as historical prices, volume data, and news sentiment.

### Data Sourcing
Our primary datasource will be the IEX Cloud API, which provides real-time & historical stock and market data. Specifically, we will fetch:

Historical price data: To analyze past trends and fluctuations.
Volume data: To gauge the trading activity and its potential influence on stock price.
News sentiment: As an exogenous variable, capturing how news sentiment impacts stock movement.
We are also open to the integration of additional indicators and metrics in the future to refine our prediction model further.

### Backtesting Strategy
Backtesting using IEX Deep Data
To validate the efficacy and robustness of our LSTM-based trading strategy by backtesting against a detailed and comprehensive set of historical market data.

1. Clean the data to remove any anomalies, outliers, or missing values. Transform the data into a format suitable for our LSTM model. Given that LSTM models require sequence data, we will convert the raw data into appropriate time series sequences.

2. Integrate the cleaned and preprocessed data into our existing PyTorch-based LSTM model. Ensure that the model considers our three initial elements: historical price data, volume data, and news sentiment. We'll leave placeholders for additional indicators that may be incorporated later.

3. Simulate the LSTM-based trading strategy over the historical data. At each time step, the model will predict the future stock price and make trading decisions based on the prediction. It will simulate trades according to the strategy's logic.

4. Compile a comprehensive report on the backtest results, focusing on key performance indicators such as total return, maximum drawdown, Sharpe ratio, win rate, and others. Visualize the strategy's performance over time with relevant plots and charts.

As of now, I have laid down a foundational PyTorch implementation employing LSTM for stock price forecasting. While the data preprocessing component has been integrated, the model's current version lacks a risk management facet. Additionally, real IEX data integration is pending due to unfamiliarity with the data specifics and IEX API intricacies. Currently, the model is designed to function using prices as the singular input.

<!-- Methodology:
Data Acquisition:

Fetch the historical DEEP data for our desired backtest range from IEX Cloud. This data will encompass real-time limit order books (OpenBook), trades, and other relevant trading activity for all IEX-listed securities.
Preparation and Preprocessing:

Clean the data to remove any anomalies, outliers, or missing values.
Transform the data into a format suitable for our LSTM model. Given that LSTM models require sequence data, we will convert the raw data into appropriate time series sequences.
Implementation:

Integrate the cleaned and preprocessed data into our existing PyTorch-based LSTM model.
Ensure that the model considers our three initial elements: historical price data, volume data, and news sentiment. We'll leave placeholders for additional indicators that may be incorporated later.
Running the Backtest:

Simulate the LSTM-based trading strategy over the historical data.
At each time step, the model will predict the future stock price and make trading decisions based on the prediction. It will simulate trades according to the strategy's logic.
Results and Analysis:

Compile a comprehensive report on the backtest results, focusing on key performance indicators such as total return, maximum drawdown, Sharpe ratio, win rate, and others.
Visualize the strategy's performance over time with relevant plots and charts.
Refinement:

Based on the insights from the backtest results, iterate on the LSTM model's architecture or the trading strategy's logic to enhance its performance.
Continuously backtest refined versions of the model to achieve an optimal configuration. -->

## Timeline 

detailed timeline of what needs to be done and when to complete your project

11/7: UDP Testing harness complete. Entire strategy designed & correct bits from iex deep asserted. ML Framework created. Vagrant lab setup

11/14: Hardware strategy development. ML Framework able to generate weights & outputs

11/21: Hardware able to generate outputs based on specific trades. SW has meaningful weights through backtesting.

12/5: Hardware able to generate meaningful outputs

12/12: Final presentation & demo

## Project Goals & Deliverables 

Bare Minumum Goals:

1. Software able to generate meaningful outputs & weights from backtesting (Diego and Elio)
2. Simulation able to import weights and generate outputs (Jason)
3. Simulation UDP testing harness with accurate timestamps (Adrian)
4. Lab setup Vagrantized (Adrian)

Expected Goals:
1. No hardware bugs through all IEX DEEP data (Jason and Adrian)
2. Software ML model very dynamic and can react to data trends (Diego and Elio)
3. Hardware can be synthesized and static timing analysis can be ran (Jason and Adrian)
4. Hardware very configureable with changing software data (Jason and Adrian)

Reach Goals:
1. Implementation on x3 FPGA in lab (all)
2. 10gb/s input (all)

