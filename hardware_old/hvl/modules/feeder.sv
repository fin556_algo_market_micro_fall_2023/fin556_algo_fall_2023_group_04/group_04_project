`define MAX_UDP_SIZE 1024

module feeder(
input logic clk,
input logic rst,
input logic [`MAX_UDP_SIZE - 1:0] parallel_load,
input logic load,
input logic read_start,

output logic d_out_valid,
output logic serial_feed
);

logic [`MAX_UDP_SIZE - 1:0] packet;

logic [31:0] read_pointer;

logic read_active;

always_ff @ (posedge clk) begin
    if(rst) begin
        packet <= '0;
        read_pointer <= '0;
        read_active <= 1'b0;
        d_out_valid <= 1'b0;
    end
    else if(read_active) begin
        if(read_pointer == `MAX_UDP_SIZE - 1) begin
            read_active <= 1'b0;
            d_out_valid <= 1'b0;
        end
        else begin
            read_pointer <= read_pointer + 1;
            d_out_valid <= 1'b1;
        end
    end
    else if(read_start) begin
        read_pointer <= '0;
        read_active <= 1'b1;
        d_out_valid <= 1'b1;
    end
    else if(load & ~read_active) begin
        packet <= parallel_load;
    end
    

end

always_comb begin
    if(d_out_valid)
        serial_feed = packet[read_pointer];
    else
        serial_feed = '0;
end






endmodule
