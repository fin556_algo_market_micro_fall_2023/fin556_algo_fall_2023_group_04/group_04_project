`timescale 1ns / 1ps


module tb_top();


//nanosecond clock
bit clk;
//microsecond clock
bit micro_clk;

initial clk = 1'b1;
initial micro_clk = 1'b1;

always #0.5 clk = ~clk;

always #500 micro_clk = ~micro_clk;


//feeder tb vars
logic rst;
logic [1023:0] parallel_load;
logic load;
logic read_start;
logic d_out_valid;
logic serial_feed;

//algo vars
logic trade;



feeder feeder(
    .clk(clk),
    .rst(rst),
    .parallel_load(parallel_load),
    .load(load),
    .read_start(read_start),
    .d_out_valid(d_out_valid),
    .serial_feed(serial_feed)
);

algo algo (
    .clk(clk),
    .rst(rst),
    .serial_in(serial_feed),
    .valid_in(d_out_valid),
    .trade(trade)

);


initial begin
    $fsdbDumpfile("dump.fsdb");
	$fsdbDumpvars(0, "+all");

    rst <= 1'b0;
    parallel_load <= 10'b0;
    load <= 1'b0;
    read_start <= 1'b0;
    #10;
    rst <= 1'b1;
    #5;
    rst <= 1'b0;
    #5;

    parallel_load <= 1024'hF03D68F59C6C6C978D9C43B397F15A7C67E905D7C49E66637A5C27946CEEB3C2AB679BE7E7E15D46C8A54C80F4A2D560CB1E7C7FA2F5AA11E1594989E612AC8CA4D228DB2D62A5E9C2AA78F88F65B8B4C56E4F6656D615CD383E13AAFF1051BFCB582F97DCCAA8EC8E156EFA84F64A25C3DC3649A92A23B9062F66E1128A980F;
    load <= 1'b1;
    #1;
    load <= 1'b0;
    #5;

    read_start <= 1'b1;
    #5;
    read_start <= 1'b0;


    

    #10000;

    $finish;

end






endmodule