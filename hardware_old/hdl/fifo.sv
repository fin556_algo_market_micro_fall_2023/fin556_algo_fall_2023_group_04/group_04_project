module fifo
(
    input logic clk_i,
    input logic reset_n_i,

    // valid-ready input protocol
    input logic data_i,
    input logic valid_i,
    output logic ready_o,

    // valid-yumi output protocol
    output logic valid_o,
    output logic data_o,
    input logic yumi_i
);

parameter WIDTH_P = 1;
parameter CAP_P = 1024;

/******************************** Declarations *******************************/
// Need memory to hold queued data
logic [WIDTH_P-1:0] queue [CAP_P-1 : 0];

// Pointers which point to the read and write ends of the queue
logic [9:0] read_ptr, write_ptr, read_ptr_next, write_ptr_next;

// Helper logic
logic msb_of;
logic empty, full, ptr_eq, sign_match;
logic  enqueue, dequeue;

// We always know what the next data which will be dequeued is.
// Thus it only makes sense to register it in an output buffer
logic [WIDTH_P-1:0] output_buffer_r;
/*****************************************************************************/

/***************************** Output Assignments ****************************/
assign ready_o = ~full;
assign valid_o = ~empty;
assign data_o = output_buffer_r;
/*****************************************************************************/

/******************************** Assignments ********************************/
assign full = ptr_eq & msb_of;
assign ptr_eq = |(read_ptr == write_ptr);
//assign sign_match = read_ptr[PTR_WIDTH_P] == write_ptr[PTR_WIDTH_P]; this is useless now, and a bad implementation to begin with
assign empty = ptr_eq & ~msb_of;
assign enqueue = ready_o & valid_i;
assign dequeue = valid_o & yumi_i;
assign write_ptr_next = write_ptr + '1;
assign read_ptr_next = read_ptr + '1;
/*****************************************************************************/

/*************************** Non-Blocking Assignments ************************/
always_ff @(posedge clk_i, negedge reset_n_i) begin
    // The `n` in the `reset_n_i` means the reset signal is active low
    if (~reset_n_i) begin
        read_ptr  <= '0;
        write_ptr <= '0;
        msb_of <= 0;
    end
    else begin
        case ({enqueue, dequeue})
            2'b00: ;
            2'b01: begin : dequeue_case
                if(msb_of) begin
                    msb_of <= 0;
                end
                output_buffer_r <= queue[read_ptr_next];
                read_ptr <= read_ptr_next;
            end
            2'b10: begin : enqueue_case
                if(write_ptr == 10'b1111111111) begin
                    msb_of <= 1;
                end
                queue[write_ptr] <= data_i;
                write_ptr <= write_ptr_next;
                if (empty) begin
                    output_buffer_r <= data_i;
                end
            end
            // When enqueing and dequeing simultaneously, we must be careful
            // to place proper data into output buffer.
            // If there is only one item in the queue, then the input data
            // Should be copied directly into the output buffer
            2'b11: begin : dequeue_and_enqueue_case
                // Dequeue portion
                output_buffer_r <= read_ptr_next == write_ptr ?
                                   data_i :
                                   queue[read_ptr_next];
                read_ptr <= read_ptr_next;

                // Enqueue portion
                queue[write_ptr] <= data_i;
                write_ptr <= write_ptr_next;
                // if(msb_of) begin
                //     msb_of <= 0;
                // end
                // if(write_ptr == 8'b11111111) begin
                //     msb_of <= 1;
                // end

                // No need to check empty, since can't dequeue from empty
            end
        endcase
    end
/*****************************************************************************/
end

endmodule 