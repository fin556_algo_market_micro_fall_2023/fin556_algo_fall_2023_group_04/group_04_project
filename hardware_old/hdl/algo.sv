`define MAX_UDP_SIZE 1024

module algo(
    input logic clk,
    input logic rst,
    input logic serial_in,
    input logic valid_in,
    output logic trade
);

logic write_en, read_en;

logic full, empty, data_out;

logic d_out;

sm sm(
    .clk(clk),
    .rst(rst),
    .valid(valid_in),
    .write_en(write_en), //to fifo
    .read_en(read_en) //to fifo
);



fifo fifo(
    .clk_i(clk),
    .reset_n_i(~rst),

    // valid-ready input protocol
    .data_i(serial_in),
    .valid_i(write_en),
    .ready_o(full),

    // valid-yumi output protocol
    .valid_o(empty),
    .data_o(d_out),
    .yumi_i(read_en)
);

assign trade = 0;


endmodule
