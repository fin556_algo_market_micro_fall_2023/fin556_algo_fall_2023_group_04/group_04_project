module sm(
    input logic clk,
    input logic rst,
    input logic valid,
    output logic write_en,
    output logic read_en
);

logic [1:0] curr_state, next_state;

parameter IDLE = 2'b00;
parameter TAKE_INPUT = 2'b01;

always_ff @ (posedge clk) begin
    if(rst)
        curr_state <= IDLE;
    else
        curr_state <= next_state;
end

always_comb begin
    case(curr_state)

    IDLE: begin
        write_en = 1'b0;
        read_en = 1'b0;

        if(valid) begin
            next_state = TAKE_INPUT;
            write_en = 1'b1;
        end

        else
            next_state = IDLE;
    end

    TAKE_INPUT: begin
        write_en = 1'b1;
        read_en = 1'b0;

        if(~valid) begin
            //write_en = 1'b0;
            next_state = IDLE;
        end
        else   
            next_state = TAKE_INPUT;
    end




    endcase


end


endmodule