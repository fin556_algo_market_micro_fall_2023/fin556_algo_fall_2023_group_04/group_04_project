//`define UDP_SIZE 16384

module algo(
    input logic clk,
    input logic rst,
    input logic [16384 - 1:0] packet_in,
    input logic valid_in,
    output logic trade,
    output logic price_level_update_seen,
    output logic ready_for_next_packet
);


//logic [7:0] d_out;

//logic [14:0] addr;


sm sm(
    .clk(clk),
    .rst(rst),
    .valid(valid_in), 
    .d_in(packet_in),
    .price_level_update_seen(price_level_update_seen),
    .ready_for_next_packet(ready_for_next_packet)
);


 

//byte addressable ff array
// ff_array ff_array(
//     .d_in(packet_in), // 16384-bit input
//     .address(addr), // 14-bit address for 16384/16 = 1024 address locations
//     .load(load),
//     .q_out(d_out) // Output from the addressed flip-flop
// );



assign trade = 0;


endmodule
