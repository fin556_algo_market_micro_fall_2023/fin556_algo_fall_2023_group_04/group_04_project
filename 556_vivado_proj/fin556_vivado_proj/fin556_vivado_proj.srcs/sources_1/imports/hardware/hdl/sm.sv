module sm(
    input logic clk,
    input logic rst,
    input logic valid,
    input logic [16383:0] d_in,
    //output logic load_array,
    //output logic [14:0] addr
    output logic price_level_update_seen,
    output logic ready_for_next_packet
);

logic [5:0] curr_state, next_state;

int num_messages;
reg [31:0] message_count;
int curr_packet_size;

logic [14:0] curr_packet_location;
logic [7:0] curr_protocol;
logic [63:0] curr_security;


logic [63:0] curr_price;

logic analysis_done;




//FF array values
logic [7:0] flip_flop_array [0:2047];
logic load_array;
logic [14:0] addr;


parameter IDLE = 6'b00000;
parameter PARSE_ARRAY = 6'b000001;
parameter PACKET_INFO = 6'b000010;
parameter PACKET_UPDATE = 6'b000011;
parameter PACKET_ANALYZE = 6'b000100;

always_ff @ (posedge clk) begin
    if(rst)
        curr_state <= IDLE;
    else
        curr_state <= next_state;
end

function void set_defaults();
    load_array = 1'b0;
    addr = 14'b0;

endfunction

always_comb begin
    case(curr_state)

    IDLE: begin
        num_messages = 0;
        curr_packet_size = 0;
        message_count = 0;
        curr_packet_location = 15'h52;
        price_level_update_seen = 1'b0;
        ready_for_next_packet = 1'b1;
        analysis_done = 1'b0;
        if(valid) begin
            next_state = PARSE_ARRAY;
            load_array = 1'b1;
        end
        else
            next_state = IDLE;
    end

    PARSE_ARRAY: begin
        load_array = 1'b0;
        ready_for_next_packet = 1'b0;
        //message count at byte x38
        num_messages = flip_flop_array[15'h38];
        //start at first message order (after header (start on byte 42))
        //look at what kind of order message it is
        if(num_messages > 0) begin
            message_count = num_messages;
            next_state = PACKET_INFO;
        end
        else begin
            next_state = IDLE;
        end
        //next_state = IDLE;
    end

    PACKET_INFO: begin
        price_level_update_seen = 1'b0;
        curr_packet_size = flip_flop_array[curr_packet_location]; //assume size not greater than xFF
        curr_protocol = flip_flop_array[curr_packet_location + 2];
        curr_price = 64'b0;

        //curr_price = 64'({<<8{{flip_flop_array[curr_packet_location + 24], flip_flop_array[curr_packet_location + 25], flip_flop_array[curr_packet_location + 26], flip_flop_array[curr_packet_location + 27], flip_flop_array[curr_packet_location + 28], flip_flop_array[curr_packet_location + 29], flip_flop_array[curr_packet_location + 30], flip_flop_array[curr_packet_location + 31]}}});
        if(curr_protocol == 8'h50) begin//short sale right now to test - PLU is in same location
            curr_security = {flip_flop_array[curr_packet_location + 12], flip_flop_array[curr_packet_location + 13], flip_flop_array[curr_packet_location + 14], flip_flop_array[curr_packet_location + 15], flip_flop_array[curr_packet_location + 16], flip_flop_array[curr_packet_location + 17], flip_flop_array[curr_packet_location + 18], flip_flop_array[curr_packet_location + 19]};
            curr_price =  {flip_flop_array[curr_packet_location + 24], flip_flop_array[curr_packet_location + 25], flip_flop_array[curr_packet_location + 26], flip_flop_array[curr_packet_location + 27], flip_flop_array[curr_packet_location + 28], flip_flop_array[curr_packet_location + 29], flip_flop_array[curr_packet_location + 30], flip_flop_array[curr_packet_location + 31]};
        end
        else
            curr_security = 32'h0;

        if((curr_protocol == 8'h35 || curr_protocol == 8'h38)) begin //security identifier ///&& flip_flop_array[curr_packet_location + 10] == 8'hFF
            price_level_update_seen = 1'b1;
            next_state = PACKET_ANALYZE;
        end
        else begin
            next_state = PACKET_UPDATE;
        end

    end

    PACKET_UPDATE: begin
        message_count = message_count - 1;
        curr_packet_location = curr_packet_location + curr_packet_size + 2;
        //if(message_count > 0)
            next_state = PACKET_INFO;
        // else
        //     next_state = IDLE;
    end



    PACKET_ANALYZE: begin


    //  curr_price =  {flip_flop_array[curr_packet_location + 24], flip_flop_array[curr_packet_location + 25], flip_flop_array[curr_packet_location + 26], flip_flop_array[curr_packet_location + 27], flip_flop_array[curr_packet_location + 28], flip_flop_array[curr_packet_location + 29], flip_flop_array[curr_packet_location + 30], flip_flop_array[curr_packet_location + 31]};

       // curr_price = 64'({<<8{}});
        
        

        if(analysis_done)
            next_state = PACKET_INFO;
        

    end




    endcase


end


//might need to make adding values regs
// always_ff @ (posedge clk) begin
//     case(curr_state)
    




//     endcase

// end


always_ff @(posedge load_array) begin
        for (int i = 0; i < 2048; i++) begin
            // Construct 16-bit chunks using concatenation
            flip_flop_array[i] <= {d_in[(i + 1) * 8 - 1], d_in[(i + 1) * 8 - 2], d_in[(i + 1) * 8 - 3], d_in[(i + 1) * 8 - 4], d_in[(i + 1) * 8 - 5], d_in[(i + 1) * 8 - 6], d_in[(i + 1) * 8 - 7], d_in[i * 8]};
        end
    end










endmodule