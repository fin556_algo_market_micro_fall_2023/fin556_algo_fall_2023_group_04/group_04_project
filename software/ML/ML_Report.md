# ML_Report

Our group choose **Recurrent Neural Networks (RNN)** as our neural network models for doing trading strategies

- Designed for sequential data, making them suitable for time series like stock prices.

To run our RNN modle make sure you have follow pacakges.

```python
import numpy as np
import pandas as pd
import plotly.express as px
import plotly.graph_objects as go2
import matplotlib.pyplot as plt
import seaborn as sns
import torch
import torch.nn as nn
import math, time
from sklearn.metrics import mean_squared_error

# from utils import get_order_book
from sklearn.preprocessing import MinMaxScaler

import os
import warnings
warnings.filterwarnings("ignore")
```

if not, please do

```python
pip install numpy pandas plotly matplotlib seaborn torch scikit-learn
```

#### Data processing:

```python
data = None
date_idx = []
date_plot_idx = []
```

```python
def get_order_book(file_dir):
    '''
    Return the orderbook by reading the file directory provided
    Parameters
    ----------
    string : file_dir
        file directory containing the text tick data information
    '''

    # read the file
    file = pd.read_csv(
        file_dir,
        dtype="str",
        usecols = np.arange(8),
        header=None,
        names=["Date1", "Date2", "id", "Type", "Source", "Mix", "Value", "Quantity"]
    ).fillna(0)

    # specifies the type
    file = file.astype({
        "id": np.int64 or 0,
        "Mix": np.float64 or 0,
        "Value": np.float64 or 0,
        "Quantity": np.int64
    })

    # Distinguish trade book and price book
    file["Price"] = file["Value"]
    file.loc[file["Type"]=="T", "Quantity"]= file[file["Type"]=="T"]["Price"]
    file.loc[file["Type"]=="T", "Price"]= file[file["Type"]=="T"]["Mix"]


    file = file.drop(columns=["Date2", "Mix", "Value"])

    trade_book = file.loc[file["Type"] == "T"]
    price_book = file.loc[file["Type"] == "P"]
    price_book["Date1"] = price_book["Date1"].apply(lambda s : s.split(" ")[1].split(".")[0])

    return trade_book, price_book
```

This function helps us to get raw order book, we still need to process IEX data to get Running average price and liquidity.

```python
for dirname, _, filenames in os.walk('Data_path'):
    filenames = sorted(filenames)
    for filename in filenames:
        path = os.path.join(dirname, filename)
        print(path)
        if not path[-4:] == ".txt":
            continue
        trade_book = get_order_book(path)
        date_idx.append(len(trade_book))
        if len(date_plot_idx) == 0:
            date_plot_idx.append(len(trade_book))
        else:
            date_plot_idx.append(len(trade_book) + date_plot_idx[-1])
        if data is None:
            data = trade_book
        else:
            data = pd.concat([data, trade_book], ignore_index=True)
```

The fisrt step for our ML part is processing the iuput IEX SPY data, reading each file, and processing it using a function called `get_order_book`.



#### Define basic (RNN)  using PyTorch:

```python
class RNN(nn.Module):
    def __init__(self, input_dim, hidden_dim, num_layers, output_dim):
        super(RNN, self).__init__()
        self.hidden_dim = hidden_dim
        self.num_layers = num_layers

        # Define the RNN layer
        self.rnn = nn.RNN(input_dim, hidden_dim, num_layers,               				 batch_first=True)
        
        # Define the fully connected (linear) layer
        self.fc = nn.Linear(hidden_dim, output_dim)

    def forward(self, x):
        h0 = torch.zeros(self.num_layers, x.size(0), 				    									self.hidden_dim).requires_grad_()
        out_norm, hn = self.rnn(x, h0.detach())
        out_norm = self.fc(out_norm[:, -1, :]) 
        return out_norm
```

- The `RNN` class is a subclass of `nn.Module`, which is the base class for all PyTorch neural network modules.
- The ‘__init__’ method is the constructor where the architecture of the network is defined. It takes several parameters:
  - `input_dim`: The number of expected features in the input.
  - `hidden_dim`: The number of features in the hidden state of the RNN.
  - `num_layers`: The number of recurrent layers in the RNN.
  - `output_dim`: The number of output features after the RNN layer.



- The `forward` method defines the forward pass of the network, which describes how input data is processed through the layers of the network.
- `x` is the input data.
- `h0` and `c0` are the initial hidden and cell states of the RNN. They are initialized as tensors filled with zeros. `requires_grad_()` is used to indicate that gradients need to be calculated for these tensors during backpropagation.
- `out_norm, (hn, cn)` is the result of passing the input `x` through the RNN layer. `out_norm` contains the output features from all time steps, and `(hn, cn)` contains the hidden and cell states after the last time step.
- `out_norm = self.fc(out_norm[:, -1, :])` applies a fully connected layer (`self.fc`) to the output features from the last time step (`out_norm[:, -1, :]`). This is a common approach when using RNNs for sequence-to-one tasks, such as time series prediction, which will be used for price predicting for us. 
- The final output is returned.

This code essentially defines a simple RNN model for sequence prediction tasks, such as predicting the next value in a time series. 