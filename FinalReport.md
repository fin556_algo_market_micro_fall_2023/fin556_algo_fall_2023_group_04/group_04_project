# Group 4 Final Report

# Hardware Implementation of Momentum Based Strategies Using Raw IEX DEEP Data

## Meet the Team:

### Jason Romps


<p align="center">
<img src="./final_report_image_sources/JasonProfile.JPG" alt="Jason Profile Picture" width="300" />
</p>

I am a Senior at UIUC studying Computer Engineering with a concentration in digital design and will be graduating in May 2024. I have lots of industry experience in FPGA & ASIC design and verification. I am the team lead for our low-latency hardware trading application. You can reach me at jasonar2@illinois.edu 

My [LinkedIn Profile](https://www.linkedin.com/in/jasonromps/)

### Diego Rapppaccioli

I am a Senior at UIUC studying Industrial Engineering with a minor in Computer Engineering and will be graduating in December. I have basic programming experience and have been able to work with large financial firms in the past. You can reach me at diegor3@illinois.edu

### Lijiajin Lin

I am a Senior at UIUC studying Computer Science concentrated in system design and will be graduating in May 2024. I have lots of experience in distributed system design. I have learned courses like Data Structures, Applied Linear Algebra, Database Systems, Computer Architecture, Algorithms Models of Computation, Operating Systems, Numerical Methods,Applied Machine Learning, Game Development. My programming experience entails with language like Java, Python, C/C++, Ruby, Git, C, MIPS Assembly, MySQL, Kotlin and frameworks like OpenCV, Flask, PyTorch, Firebase, TensorFlow, Spring, Django, PostgreSQL, MongoDB, Docker, Pandas, Numpy.

You can reach me at llin17@illinois.edu
My [LinkedIn Profile](https://www.linkedin.com/in/lijiajin-lin-602b32200/)

### Adrian Cheng

<p align="center">
<img src="./final_report_image_sources/AdrianProfile.jpg" alt="Adrian Profile Picture" width="300" />
</p>
I am a Senior at UIUC studying Computer Engineering graduating in December 2023. I have industry experience in full stack development and I have experience in created automated deployments in a variety of environments. I currently going to pursure a Masters degree in Financial Engineering with a specific focus in hardware design. I have extensive experience in digital design using System Verilog as well as working with RISCV Systems.

You can reach me at emails: acheng27@illinois.edu, bluepop11@hotmail.com

My [LinkedIn Profile](https://www.linkedin.com/in/adrianpccheng/)

# Strategy Development

**Why Technical Analysis and Moving Averages:**

In the realm of technical trading, the moving average (MA) is known for its ability to distill short- to medium-term trade signals from the incessant noise of market price fluctuations. It is not merely a tool for trend identification but serves as a compass guiding traders through market volatility.

The concept of MA is both fundamental and versatile within technical analysis. As an indicator, it calculates the average asset price over a designated period, effectively ironing out erratic price movements to reveal an underlying trend. While there are various types of MAs, each with its nuances, they all share the common purpose of providing traders with a smoothed representation of price action.

The application of MAs in trading is multifaceted. In an uptrend, the MA can offer a level of support, acting as a springboard for ascending prices. Conversely, during downtrends, it may pose as a form of resistance, reflecting a ceiling which prices struggle to breach. Calculating an MA is a matter of arithmetic—summing up a collection of prices over a time frame and dividing by the number of prices in that series.

The practical usage of MAs extends beyond mere calculations. They form the backbone of many trading strategies, including the renowned crossover method. This method involves observing the interaction between short-term and long-term Mas and prices to signal potential entry and exit points.

The team, cognizant of the intricate details of market microstructure, recognized an opportunity to leverage the precision of IEX market data—remarkably detailed to seven decimal points in the time stamps of price data. This granularity is crucial when considering the micro-movements in pricing, which can signal the inception of a new trend or the exhaustion of an existing one. The convergence of this detailed data with the prowess of FPGA hardware presents a compelling synergy for the implementation of a moving average crossover strategy.

By employing FPGA technology, the team can compute MAs at speeds that far surpass conventional software applications. This rapid calculation, aligned with the minute precision of IEX data, allows for an agile response to market movements, enabling the team to capitalize on fluctuations that might be imperceptible to less-equipped traders.

The project's premise is founded on the hypothesis that the blending of FPGA's computational speed with the ultra-precise time-stamped data can yield a crossover strategy that is not only viable but advantageous. It is an approach that marries the depth of market understanding with the technical capability to act swiftly, a combination poised to exploit the fleeting inefficiencies that arise within the market's microstructure.

In essence, the team’s strategy is a mix of technical sophistication and market acumen. It transcends traditional analysis by utilizing a hardware-enhanced approach to dissect and interpret the complex signals inherent in financial data.

**Hardware Advantages:**
Our team focused on the unique ability of hardware to analyze and calculate various parameters from exchange data at speeds surpassing any software solution. This speed advantage is critical for making timely trades and positioning based on market signals. We brainstormed strategies that would leverage this hardware capability to perform rapid calculations based on incoming market data.

**Selection of Securities:**
Considering the IEX exchange's offerings, we zeroed in on highly liquid securities, particularly the S&P 500 ETF (SPY). High liquidity translates to more data points, making SPY an ideal candidate for high-frequency data strategies that can be effectively implemented in hardware.

**Technical Analysis Strategy:**

Our strategy utilized the price_level_update protocol in IEX's PCAP data files, observed approximately 200,000 times in a single trading day. We decided on a technical analysis approach that computes moving averages for every price level update. The team knew that hardware's capability to calculate moving averages would be extremely useful in supporting the generation of buy and sell signals. This method is well-suited for hardware implementation due to the high frequency of data points, enabling swift calculation of moving averages. The strategy primarily focused on identifying moments where these moving averages were crossed by the current price found in the price_level_update protocols, exploiting these moments as potential buy or sell signals.

![Alt text](final_report_image_sources/Initial_Trading_Strategy_2.0.jpg)

**Figure 1:Main idea behind initial trading strategy vision**

**Implementing the Strategy:**

**Data Parsing:**
The first step in building the strategy was parsing the IEX data. The DEEP PCAP file format, which is storage-heavy and contains timestamps for different messages in binary data representing various protocols from the IEX exchange, was our primary data source. Our goal was to parse this data to collect the SPY and corresponding price_level_update from each distinct market data.

**Data Collection Process:**
Our data collection involved downloading market data into a large SSD, covering various trading days. Utilizing the iex_parser library by Prof. David Lariviere and several other libraries, we developed a Python script to parse the downloaded PCAP files, focusing on the SPY security. This script facilitated the generation of CSV files containing initial buy signals and was named “November_Data_Collection.ipynb,” found in our project repository.

**Parsing Code Analysis and Explanation:**
The Python script, "November_Data_Collection.ipynb," performed several key functions:

**File Parsing and Date Extraction:** It starts by parsing a DEEP PCAP file containing market data. Using a regular expression, it extracts the date from the file name and sets up an output CSV file.
CSV File Initialization: The script initializes a CSV file with predefined fields, including 'type', 'timestamp', 'symbol', 'size', 'price', 'daily_moving_average', 'above_daily_moving_average', and 'has_price_gone_up_yet'.
Data Processing Loop: For each message in the PCAP file related to the SPY symbol, the script captures the timestamp and converts it to a string. It tracks the trading day, resetting variables at the start of a new day. For 'price_level_update' messages, it captures size and price, and after accumulating 30 price updates, calculates the 30-point moving average. It then checks if the current price is above this moving average, updating the 'above_daily_moving_average' and 'has_price_gone_up_yet' columns accordingly.

**Moving Average and Tracking:**
For each 'price_level_update' message related to SPY, the script records the price and calculates the 30-point moving average after 30 updates. It tracks if the current price is above this average and notes the first crossing above it on a trading day.

**CSV Output:**
Each processed message, along with calculated indicators such as the daily moving average and whether the price is above this average, is written to a row in the CSV file. This methodical approach in scripting and data processing is key to our strategy, leveraging the swift computational capabilities of hardware to analyze market trends in real time. An example of the CSV parsed data is in the git project’s repository as market_data_generated_example.csv.

![Alt text](final_report_image_sources/Initial_Parsig_of_DEEP_Data_CSV_Output_Example.png)

**Figure 2: CSV File Generated by Python Script**

Script created by team to parse IEX market Data:

```python
import csv
import re
from iex_parser import Parser, TOPS_1_6
from datetime import datetime, timedelta

TOPS_SAMPLE_DATA_FILE = '20231110_IEXTP1_DEEP1.0.pcap'

## Extracting the date from the file name using a regular expression.
date_match = re.search(r'\d{8}', TOPS_SAMPLE_DATA_FILE)
if date_match:
    date_str = date_match.group(0)
    output_file = f'market_data_{date_str}.csv'
else:
    output_file = 'market_data.csv'

## Define the fields for the output CSV file.
fields = ['type', 'timestamp', 'symbol', 'size', 'price', 'daily_moving_average', 'above_daily_moving_average', 'has_price_gone_up_yet']

## Initialize variables for the trading strategy
start_of_day = None
prices = []
has_price_gone_up_yet = 0  # This will track if the price has gone up above the daily moving average at least once
price_update_count = 0  # Counts the number of price updates

## Store the current trading day's timestamp
current_trading_day = None

with Parser(TOPS_SAMPLE_DATA_FILE, TOPS_1_6) as reader:
    with open(output_file, mode='w', newline='') as file:
        writer = csv.DictWriter(file, fieldnames=fields)
        writer.writeheader()

        for message in reader:
            if 'symbol' in message and message['symbol'] == b'SPY':
                # Extract the timestamp
                timestamp = message['timestamp']
                timestamp_str = timestamp.strftime('%Y-%m-%dT%H:%M:%S.%fZ')
                symbol = message['symbol'].decode('utf-8')

                # Check if a new trading day has started based on the date of the message
                message_date = timestamp.date()
                if current_trading_day is None or current_trading_day != message_date:
                    current_trading_day = message_date
                    prices = []
                    has_price_gone_up_yet = 0  # Reset for the new trading day
                    price_update_count = 0  # Reset price update count for the new trading day

                if message['type'] == 'price_level_update':
                    size = message['size']
                    price = message['price']
                    prices.append(price)
                    price_update_count += 1  # Increment the price update count

                    if price_update_count > 30:
                        daily_moving_average = sum(prices[-30:]) / 30  # Calculate the average of the last 30 prices
                        is_above_daily_moving_average = price > daily_moving_average

                        # Check if price has just exceeded the daily moving average and `has_price_gone_up_yet` is still 0
                        if is_above_daily_moving_average and has_price_gone_up_yet == 0:
                            above_daily_moving_average = 1
                            has_price_gone_up_yet = 1  # Set this to 1 as the price has now gone above the moving average
                        else:
                            above_daily_moving_average = 0

                        # If the price goes below the daily moving average, reset `has_price_gone_up_yet`
                        if not is_above_daily_moving_average:
                            has_price_gone_up_yet = 0
                    else:
                        # Default values before 30 price updates
                        daily_moving_average = None
                        above_daily_moving_average = 0

                else:
                    # Handle non-price level update messages or other symbols
                    size = None
                    price = None
                    daily_moving_average = None
                    above_daily_moving_average = 0

                # Define the row within the scope where all variables are defined
                row = {
                    'type': message['type'],
                    'timestamp': timestamp_str,
                    'symbol': symbol,
                    'size': size,
                    'price': price,
                    'daily_moving_average': daily_moving_average,
                    'above_daily_moving_average': above_daily_moving_average,
                    'has_price_gone_up_yet': has_price_gone_up_yet
                }

                writer.writerow(row)
```

**Why the Team Evolved the Strategy:**

The initial strategy, while robust in its logic and suitable for a proof-of-concept phase, presented certain limitations when confronted with the dynamic and complex nature of financial markets. The simplicity of using a daily moving average in comparison to real-time price levels to generate buy or sell signals was an efficient starting point. However, the strategy did not fully exploit the advanced computational capabilities and rapid arithmetic processing afforded by FPGA technology.

The premise of harnessing hardware like FPGA lies in its ability to perform high-speed calculations and data processing which can be pivotal in executing trades with precision and minimal latency. The original implementation, focused solely on the daily moving average crossover with current prices, though effective in theory, underutilized the potential of hardware acceleration. Such a simplistic approach risked leaving valuable insights on the table, insights that could be captured through a more granular and complex analysis of price movements.

To transcend these constraints, the team proposed an enhancement of the original strategy by incorporating additional moving averages and integrating a more layered technical analysis framework. This evolved strategy aimed to leverage multiple time frames and a variety of technical indicators, thereby providing a multi-dimensional view of market trends and potential pivot points.

The inclusion of short-term, medium-term, and long-term moving averages, along with other technical indicators, allowed for a more nuanced understanding of market momentum and volatility.

The refined strategy also took into consideration the speed and depth of market data provided by IEX. With high-resolution timestamps, the team could dissect the market microstructure more effectively, identifying opportunities that are only visible through the lens of rapid and complex data analysis.

The new final strategy, depicted in Figure 3, encompasses a sophisticated algorithm that synthesizes multiple data points to formulate trading signals. This approach is not only more congruent with the capabilities of FPGA technology but also sets the stage for a distinctive competitive edge.

The forthcoming section on Final Strategy Implementation will delve into the intricate details of this advanced strategy. It will elucidate the specific technical indicators employed, the rationale behind their selection, and how they collectively contribute to a refined trading methodology that is both innovative and responsive to the fast-paced trading environment.

![Alt text](final_report_image_sources/Final_Trading_Strategy_2.0.jpg)

**Figure 3 Visualization of Actual Implemented Trading Strategy**

# Hardware Development

The main purpose of the development of hardware algorithms is to reduce overall latency between receiving data and sending out data. We designed our board targeting the Alveo x3522 FPGA with easy interfaces for the user to plug and play algorithms. We also have developed a performance indicator to compare latencies between different algorithms.

## Interface Overview

The interface links the state machine with the current algorithm together. They interact over a simple AXI-4 stream bus allowing for back and forward propagation. The state machine interacts with the harness by signaling it is ready for a new packet. While it is reading the individual messages in the packet it calls the algorithm module, and passes it the current price and protocol (buy/sell). The algorithm module was designed for resuseability, for the user to easily develop new strategies without having to deal with the state machine interface.

![Local Image](./final_report_image_sources/interface_ss.png)
The above waveform shows the state machine processing a specific packet at the epoch timestamp in the first row. The message at index 14 in the current packet contains a SPY buy transaction at the price of $458.73. Since this algorithm is targeting the SPY security the values are passed to the algorithm to perform computation. In this example, this dummy algorithm is purely combinational, tieing algo_valid always high. The sm_valid signal is set high on when the the transaction of values between the state machine and the algorithm happens.

Other messages in this specific packet contain other securities and other protocols that our algorithm does not care about, so the state machine will continue to process.

## State Machine Design

### State Overviews

**STARTUP:** Resets all registers and count values. Transition to IDLE on next cycle

**IDLE:** Wait until a packet comes in with the valid_packet input. It is common to spend most of the time here waiting for the next packet. Transition to PARSE_ARRAY on a packet coming in.

**PARSE_ARRAY:** Load the packet's data into the parallel read/write flip flop array. Combinationaly read the number of IEX messages to determine if the packet has data in it. This data will always reside at the 38th byte for IEX DEEP data. Transition to IDLE if empty, transition to PARSE_ARRAY if data.

**PACKET_INFO:** Start at N = 0, and read the Nth packet from the flip flop array. Get the current message's size, protocol, and price. The packet size will always be the first byte of the current message, and the protocol will always be the third byte of the current message. The current security is found by concatenating bytes 10 - 18. The price is found by concatenating bytes 22 - 30, and then doing an endianness conversion on the value. Converting the value to decimal will give the literal value of the price, where the bottom 4 digits refer to the number after the decimal point. For example, 397000 is equal to $39.70. If the current protocol is to buy or sell, and the current security matches the security that the algorithm has specifed, transition to PACKET_ANALYSIS, otherwise go to PACKET_UPDATE.

**PACKET_ANALYSIS:** If the algorithm is via the axi bus between the state machine and the algorithm module, transition to the ALGO_RUNNING state and send a valid signal to the algorithm to start executing, otherwise wait in this state until the algo is ready. The current price and the protocol is sent to the algorithm.

**ALGO_RUNNING:** The algorithm will execute its desired functionality. This can be combinationaly or sequentially. When the algorithm is done computing it will send an algo_valid signal to transition the state machine to PACKET_UPDATE.

**PACKET_UPDATE:** Update to N + 1 packet by reading the next values in the flip flop array. If we have reached the last message in the current packet, transition to PACKET_UPDATE, else transition to PACKET_INFO and begin processing the next message in the current packet.

![Local Image](./final_report_image_sources/sm_sm.png)

## Implementation of Momentum Based Algorithms

Our final trading strategy that we researched this semester was done by compaing mulitiple moving average values to find momentum in a stock's price rising or falling and execute an order based on that. View secion Final Strategy Implementation and Backtesting to read more about the actual strategy. Our algoritm is comparing the values of the daily moving average, the moving average over the previous 100 data points, and the moving average over the previous 1000 data points. Whenever the state machine sends a valid signal to the algorithm, it means a valid SPY buy or sell transaction has gone through, and the current price from the statemachine should update the algorithm's moving averages.

![Local Image](./final_report_image_sources/algo_ss.png)

![algo Image](./final_report_image_sources/algolarge.png)
The waveform above depicts the momentum based algorithm functional across 50,000 SPY targeted network packet activity.

![start of algo Image](./final_report_image_sources/algostartidle.png)
A zoomed in version near the start. Notice that prior to acquiring the first 1000 datapoints, the algorithm will remain in the idle state.

![zoomed algo Image](./final_report_image_sources/algozoom.png)
A detailed look at the individual buy and sell signals given out from the state machine.

The daily moving average is calculated from just a running total between the count and daily total, however, the previous 100 and previous 1000 are created through a paramertized parallel read-out FIFO. The FIFO will combinationaly output the average of all the values currently stored in it. One corner case with our algoritm is it is not valid until the first 1000 SPY packets have gone through, in order to get a meaningful data point for the 1000 point moving average. A very simple state machine was designed to keep track which moving averages the current price has gone above and below of.

![algorithm state machine](./final_report_image_sources/algo_sm.png)

The daily moving average algorithm will send the algo_valid signal immeditely, as only 1 clock cycle is required to update the internal FIFOs and calculate what decision should be made.

## Raw Data Collection

Data was downlaoded from [IEX DEEP's archives](https://iextrading.com/trading/market-data/#hist-download) and the pcap files were analyzed in Wireshark. Wireshark allowed us to easily debug specific timestamps of packets to make sure we were seeing correct data in our algorithm. Our original design process was to use Wireshark to convert specific ranges of data point into .json files, which we then created a python script to convert that into a memory dump.

Here is an example output the the JSON output by wireshark or the following tshark command:

`tshark -T jsonraw -J "frame.pcaplog.data" -x -c [number of packets] -r [pcap filename]`

```json
[
  {
    "_index": "packets-2023-12-06",
    "_type": "doc",
    "_score": null,
    "_source": {
      "layers": {
        "frame_raw": [
          "01005e571504b8599ff92d530800450005d5000000004011c2d617e29b84e9d71504288a288a05c149a101000480010000000000f14c910543001fe70500000000001e470000000000007fefc815e03d9e1713005000cb7ec815e03d9e1747534c2d422020202012004f4ecb7ec815e03d9e1747534c2d4220202016004854cb7ec815e03d9e1747534c2d422020202020202012004920cb7ec815e03d9e1747534c2d4220202013005000c982c815e03d9e1747534c43202020202012004f4ec982c815e03d9e1747534c432020202016004854c982c815e03d9e1747534c43202020202020202012004920c982c815e03d9e1747534c4320202020130050000b8bc815e03d9e1747534d20202020202012004f4e0b8bc815e03d9e1747534d2020202020160048540b8bc815e03d9e1747534d202020202020202020120049200b8bc815e03d9e1747534d2020202020130050004c90c815e03d9e1747534d47572020202012004f4e4c90c815e03d9e1747534d4757202020160048544c90c815e03d9e1747534d475720202020202020120049204c90c815e03d9e1747534d4757202020130050005296c815e03d9e1747535059202020202012004f4e5296c815e03d9e174753505920202020160048545296c815e03d9e17475350592020202020202020120049205296c815e03d9e17475350592020202013005000dc9bc815e03d9e1747535343202020202012004f4edc9bc815e03d9e17475353432020202016004854dc9bc815e03d9e1747535343202020202020202012004920dc9bc815e03d9e1747535343202020201300500005a6c815e03d9e1747535354202020202012004f4e05a6c815e03d9e1747535354202020201600485405a6c815e03d9e174753535420202020202020201200492005a6c815e03d9e1747535354202020201300500073b0c815e03d9e174753554e202020202012004f4e73b0c815e03d9e174753554e202020201600485473b0c815e03d9e174753554e20202020202020201200492073b0c815e03d9e174753554e2020202013005000f2b7c815e03d9e1747535553202020202012004f4ef2b7c815e03d9e17475355532020202016004854f2b7c815e03d9e1747535553202020202020202012004920f2b7c815e03d9e174753555320202020130050008ebec815e03d9e1747535920202020202012004f4e8ebec815e03d9e174753592020202020160048548ebec815e03d9e17475359202020202020202020120049208ebec815e03d9e1747535920202020201300500088c3c815e03d9e1747542020202020202012004f4e88c3c815e03d9e1747542020202020201600485488c3c815e03d9e174754202020202020202020201200492088c3c815e03d9e1747542020202020201300500077cbc815e03d9e1747544143202020202012004f4e77cbc815e03d9e1747544143202020201600485477cbc815e03d9e174754414320202020202020201200492077cbc815e03d9e174754414320202020130050003dd2c815e03d9e1747544143552020202012004f4e3dd2c815e03d9e174754414355202020160048543dd2c815e03d9e17475441435520202020202020120049203dd2c815e03d9e17475441435520202013005000e0d7c815e03d9e1747544143572020202012004f4ee0d7c815e03d9e17475441435720202016004854e0d7c815e03d9e1747544143572020202020202012004920e0d7c815e03d9e174754414357202020130050004de1c815e03d9e1747544250202020202012004f4e4de1c815e03d9e174754425020202020160048544de1c815e03d9e17475442502020202020202020120049204de1c815e03d9e174754425020202020130050008fe3c815e03d9e1747544520202020202012004f4e8fe3c815e03d9e174754452020202020160048548fe3c815e03d9e17475445202020202020202020120049208fe3c815e03d9e174754452020202020130050007fefc815e03d9e1747544543202020202012004f4e7fefc815e03d9e174754454320202020160048547fefc815e03d9e17475445432020202020202020",
          0,
          1507,
          0,
          1
        ],
        "frame": {
          "filtered": "frame"
        },
        "eth_raw": ["01005e571504b8599ff92d530800", 0, 14, 0, 1],
        "eth": {
          "filtered": "eth"
        },
        "ip_raw": ["450005d5000000004011c2d617e29b84e9d71504", 14, 20, 0, 1],
        "ip": {
          "filtered": "ip"
        },
        "udp_raw": ["288a288a05c149a1", 34, 8, 0, 1],
        "udp": {
          "filtered": "udp"
        },
        "_ws.lua.fake_raw": ["", 42, 0, 0, 0],
        "_ws.lua.fake_raw": ["", 42, 0, 0, 0],
        "iex.equities.deep.iextp.v1.0.lua_raw": [
          "01000480010000000000f14c910543001fe70500000000001e470000000000007fefc815e03d9e1713005000cb7ec815e03d9e1747534c2d422020202012004f4ecb7ec815e03d9e1747534c2d4220202016004854cb7ec815e03d9e1747534c2d422020202020202012004920cb7ec815e03d9e1747534c2d4220202013005000c982c815e03d9e1747534c43202020202012004f4ec982c815e03d9e1747534c432020202016004854c982c815e03d9e1747534c43202020202020202012004920c982c815e03d9e1747534c4320202020130050000b8bc815e03d9e1747534d20202020202012004f4e0b8bc815e03d9e1747534d2020202020160048540b8bc815e03d9e1747534d202020202020202020120049200b8bc815e03d9e1747534d2020202020130050004c90c815e03d9e1747534d47572020202012004f4e4c90c815e03d9e1747534d4757202020160048544c90c815e03d9e1747534d475720202020202020120049204c90c815e03d9e1747534d4757202020130050005296c815e03d9e1747535059202020202012004f4e5296c815e03d9e174753505920202020160048545296c815e03d9e17475350592020202020202020120049205296c815e03d9e17475350592020202013005000dc9bc815e03d9e1747535343202020202012004f4edc9bc815e03d9e17475353432020202016004854dc9bc815e03d9e1747535343202020202020202012004920dc9bc815e03d9e1747535343202020201300500005a6c815e03d9e1747535354202020202012004f4e05a6c815e03d9e1747535354202020201600485405a6c815e03d9e174753535420202020202020201200492005a6c815e03d9e1747535354202020201300500073b0c815e03d9e174753554e202020202012004f4e73b0c815e03d9e174753554e202020201600485473b0c815e03d9e174753554e20202020202020201200492073b0c815e03d9e174753554e2020202013005000f2b7c815e03d9e1747535553202020202012004f4ef2b7c815e03d9e17475355532020202016004854f2b7c815e03d9e1747535553202020202020202012004920f2b7c815e03d9e174753555320202020130050008ebec815e03d9e1747535920202020202012004f4e8ebec815e03d9e174753592020202020160048548ebec815e03d9e17475359202020202020202020120049208ebec815e03d9e1747535920202020201300500088c3c815e03d9e1747542020202020202012004f4e88c3c815e03d9e1747542020202020201600485488c3c815e03d9e174754202020202020202020201200492088c3c815e03d9e1747542020202020201300500077cbc815e03d9e1747544143202020202012004f4e77cbc815e03d9e1747544143202020201600485477cbc815e03d9e174754414320202020202020201200492077cbc815e03d9e174754414320202020130050003dd2c815e03d9e1747544143552020202012004f4e3dd2c815e03d9e174754414355202020160048543dd2c815e03d9e17475441435520202020202020120049203dd2c815e03d9e17475441435520202013005000e0d7c815e03d9e1747544143572020202012004f4ee0d7c815e03d9e17475441435720202016004854e0d7c815e03d9e1747544143572020202020202012004920e0d7c815e03d9e174754414357202020130050004de1c815e03d9e1747544250202020202012004f4e4de1c815e03d9e174754425020202020160048544de1c815e03d9e17475442502020202020202020120049204de1c815e03d9e174754425020202020130050008fe3c815e03d9e1747544520202020202012004f4e8fe3c815e03d9e174754452020202020160048548fe3c815e03d9e17475445202020202020202020120049208fe3c815e03d9e174754452020202020130050007fefc815e03d9e1747544543202020202012004f4e7fefc815e03d9e174754454320202020160048547fefc815e03d9e17475445432020202020202020",
          42,
          1465,
          0,
          1
        ],
        "iex.equities.deep.iextp.v1.0.lua": {
          "filtered": "iex.equities.deep.iextp.v1.0.lua"
        }
      }
    }
  }
]
```

This JSON file is then used to extract the raw hex dump of each packet and copyed in byte reversed order (for little endian encoding) so that when the system verilog testbench reads the packet data memory dump, it reads it in the correct order. The byte reversal code looks something like the following:

```python
outfile.write("".join(reversed([r[i:i+2] for i in range(0, len(r), 2)])) + "\n")
```

This line of code is located within `dumpgen.py`.

However, we have decided to move away from the dedicated parser to convert pcap files that the IEX DEEP data provides to verilog memory dumps as the pcap files start off extremely large. The significance of this is that if we were to convert even just a single pcap file from IEX to a memory dump, the file sizes will quickly explode. To resolve this issue we have decided to create a dedicated parser within system verilog to parse the pcap files directly.

## Testbench Design & Reuseability

**SystemVerilog PCAP Parsing**

In order to make our testbench harness more reusable and extensible for future groups who do decide to use our testbench, we have integrated several features to better navigate and simulate the hardware based trading algorithms. The first of which is the ability to parse PCAP files that has been downloaded directly from IEX. The idea behind the pcap parsing is very simple, as PCAP files simply binaries that include the packets one after the other, we used system verilog packed structs to dissasemble the binary back into the original packets.

```verilog
typedef struct packed {
    logic [31:0] magic_number;
    logic [15:0] major_version;
    logic [15:0] minor_version;
    logic [63:0] reserved;
    logic [31:0] snaplen;
    logic [2:0] frame_check_sequence;
    logic f;
    logic [11:0] zeroes;
    logic [15:0] link_type;
} pcap_file_header;

typedef struct packed {
    logic [31:0] timestamp_s;
    logic [31:0] timestamp_ns;
    logic [31:0] capture_length;
    logic [31:0] original_length;
} pcap_packet_header;
```

After the header values has been retrieved from the pcap binary, we would simply request the next bytes of the binary until the current packet has been exhausted, in which we would output that as the current packet described in the following task construct:

```verilog
task read_next_packet();
    next_packet = '0;
    next_packet_idx++;
    $fread(pkt_hdr, pcap_fd);
    packet_length = int'({<<8{pkt_hdr.capture_length}});
    for (int i = 0; i < packet_length; i++) begin
        $fread(next_packet[(i*8)+:8], pcap_fd);
    end
    next_ts = next_packet[655:592];
endtask : read_next_packet
```

Again, since everything in the pcap data is stored in little endian, we used the system verilog's streaming operator to efficiently do byte reversals in order to obtain the correcty packet lengths. the bit range near the bottom: `[655:592]` is the specific fixed bit range of each iex packet that displays when, in nanoseconds, is that packet received.

**Timing & Time Skipping Capability**

In order to have this simulation be as close to real life as possible, we have an internal nanosecond clock acting as our EPOCH time. Packets are sent through the harness when their EPOCH timestamp matches our simulations "real-time".

Since the testbench uses a nanosecond clock, we made it so that the testbench would be able to achieve nanosecond level accuracy in replaying the trade packets indicated within the IEX packet capture files. However, this also poses an issue when simulating. As shown below, because the simulation would have to wait until the correct timestamp of the next packet in order to send that one through to our state machine, there will be massive gaps in simulation where our hardware is effectively on idle waiting for the next packet to come through:

Without Time Skipping
![notimeskip](./final_report_image_sources/notimeskip.png)

With Time Skipping
![timeskip](./final_report_image_sources/timeskip.png)

The two waveforms above span across 10,000,000 nanoseconds, where the top waveform simulates the correct timings as how the packets would've been received in real time, compared to the bottom where time skipping is enabled. How time skipping functions is that instead of waiting on our real time clock to arrive at the correct nanosecond timestamp and then sending out the next packet to the state machine, we would immediately send out the next packet as soon as the state machine is ready to receive the next packet. Since IEX packets would group multiple orders into a single packet if they come within the closest microsecond, our trading algorithm which operates at an average of 5 cycles (nanoseconds) is more than fast enough to handle every packet that comes through. The main advantage of the time skip is that we are able to backtest significantly more packets compared to the real time clock. Compared to the 36 packets are are processed using the real time clock, we are able to process over 50,000 packets in the same timeframe using time skipping.

**Parameterizability of the Testing Harness**

We have also developed the testing harness to be very quickly customizable. This way it is much easier to identify how the hardware trading algorithm would respond in various packet regions as well as to find any hardware bugs that may arise when backtesting. Here are the following parameters that can be very quickly changed within our testing harness:

```verilog
parameter START_PACKET_IDX = 0;
parameter END_PACKET_IDX = 250000;
parameter TIME_SKIP = 1;
parameter SIM_LENGTH_NS = 10000000;
parameter PROGRESS_REPORT = 1;
parameter PROGRESS_DENOMINATION = 5000;

/* The line below is where you can change the path to the PCAP file simulated */
pcap_fd = $fopen("path/to/pcap/file.pcap", "r");
```

Here's an explanation on what each of the parameters do

- `START_PACKET_IDX` - Indicates the starting index of the packet you want to the simulation to start at. This index is inclusive.
- `END_PACKET_IDX` - Indicates the ending index of the packet you want the simulation to stop at. This index is exclusive.
- `TIME_SKIP` - 1 for enable, 0 for disable, this parameter allows the time skipping capability to be enabled or disabled depending on what needs to be tested.
- `SIM_LENGTH_NS` - Indicates the maximum running length of the simulation in nanoseconds.
- `PROGRESS_REPORT` - Enables console logging of the current simulation progress in the case that the simulation takes a very long time. The logs will print out the **index** at which the simulation has completed up to.
- `PROGRESS_DENOMINATION` - If PROGRESS_REPORT is set to 1, this parameter allows changing how often the console will log it's progress. For example, if this setting is set to 5000, it will log every 5000 packets it has completed.

## IEEE 754 32-bit Floating Point Extensibility

The IEX DEEP protocol stores all of its pricing data in a fixed point decimal format. However, this format isn't always the most convienient when running strategies that use a lot of computaion. This is why we also have added a specific module to convert the pricing format of IEX DEEP to the standardized IEEE 754 32 bit floating number representation. This way any generalized algorithm can function using our harness and state machine. For example, strategies that require machine learning will require lots of matrix multiplication. Keeping values in IEEE754 allows for much faster computation times when multiplying numbers together.

```verilog
module conv_price_fp32 (
    input logic [63:0] din,
    output logic [31:0] dout
);
    logic [63:0] integral_din;
    logic [31:0] fractional_din;
    logic [31:0] fractional_dout;

    assign integral_din = din / 10000;
    assign fractional_din = din % 10000;

    int norm_amt, frac_idx;
    int cum_mul, mant_idx;
    logic [95:0] mantissa_concat;
    logic [22:0] mantissa;
    logic [7:0] exponent;

    always_comb begin : fractional_calc
        norm_amt = 0;
        cum_mul = fractional_din;
        fractional_dout = 32'b0;
        for (int i = 31; i >= 0; i--) begin
            norm_amt++;
            cum_mul *= 2;
            fractional_dout[i] = cum_mul >= 10000 ? 1'b1 : 1'b0;
            cum_mul = cum_mul >= 10000 ? cum_mul - 10000 : cum_mul;
            if (cum_mul == 0)
                break;
        end
        frac_idx = 0;
        for ( int i = 31; i >= 0; i--) begin
            frac_idx++;
            if(fractional_dout[i] == 1'b1)
                break;
        end

        norm_amt = frac_idx;
        frac_idx = 0;
        for (int i = 0; i < 64; i++) begin
            if (integral_din[i])
                frac_idx = i;
        end

        norm_amt = frac_idx == 0 ? norm_amt : frac_idx;
        mantissa_concat = {integral_din, fractional_dout};

        mant_idx = 0;
        for (int i = 95; i >= 0; i--) begin
            if (mantissa_concat[i] == 1'b1) begin
                mant_idx = i;
                break;
            end
        end

        mantissa = mantissa_concat[(mant_idx-1)-:23];
        if(norm_amt == frac_idx)
            exponent = 8'(127 + norm_amt);
        else
            exponent = 8'(127 - norm_amt);

        dout = din == 0 ? 32'b0 : {1'b0, exponent, mantissa};
    end : fractional_calc
endmodule : conv_price_fp32

```

## Future Uses

Like mentioned above, the state machine is only looking for buy/sell protocols of a specific security. This is an easily configurable option, and the state machine could be configured to parse for multiple securities, and multiple protocols. Some other example protocols can be found in the [IEX DEEP specification](https://assets-global.website-files.com/635ad1b3d188c10deb1ebcba/63bd4a1cb0d2bef3cbf36bcc_IEX%20DEEP%20Specification%20v1.08.pdf). Messages that relate the halts or the retail liquidity indicator messages could result in some very dynamic algorithms. Another component that can be incorported is the size of the order. Our strategy only deals with price updates, but other strategies could use the size of the order as indicators.

As continuously mentioned, this module was designed for reuseability, for future researchers in hardware algorithm development, this repository will be extrememly useful in very easily testing out new algorithms.

**Fully Connected Neural Network in Hardware**

We have also explored the implementation of a fully connected neural network within verilog. We first explored the different ways of simplifying the different weights and biases within the calculations for forward propagation and found that pytorch has a effective tool called quantization of neural networks which effectively converts the weights into integer format. This can also be done after a neural network has been trained, without losing much prediction accuracy.

We have experiemented with the following test FNN (Fully Connected Neural Network)

```python
class TestModel(nn.Module):
    def __init__(self):
        super(TestModel, self).__init__()
        self.l1 = nn.Linear(4,32)
        self.r1 = nn.ReLU()
        self.l2 = nn.Linear(32,32)
        self.r2 = nn.ReLU()
        self.l3 = nn.Linear(32, 3)
        self.sm1 = nn.Softmax(dim=1)
    def forward(self, x):
        x = self.l1(x)
        x = self.r1(x)
        x = self.l2(x)
        x = self.r2(x)
        x = self.l3(x)
        x = self.sm1(x)
        return x
```

Without quanitzation, the weights will look like the following:

```python
{'l1': tensor([[ -93.4615,    6.7962,   85.8619,  -64.4242,   29.5484,  -47.0892,
           -52.0956,  -28.0875,   -7.8382,  -54.6357,   -3.9038,  -63.2733,
            47.0331,  -31.7753,   83.6361,  -38.2762,   32.9848,   31.0663,
          -102.1852,   88.0875,  -37.7813,   80.1611,  -52.0817,    9.3660,
            86.6330,   57.0815,   86.7324,  104.1477,  -65.9068,  -97.3591,
           -73.1799,   65.4337]]),
 'r1': tensor([[  0.0000,   6.7962,  85.8619,   0.0000,  29.5484,   0.0000,   0.0000,
            0.0000,   0.0000,   0.0000,   0.0000,   0.0000,  47.0331,   0.0000,
           83.6361,   0.0000,  32.9848,  31.0663,   0.0000,  88.0875,   0.0000,
           80.1611,   0.0000,   9.3660,  86.6330,  57.0815,  86.7324, 104.1477,
            0.0000,   0.0000,   0.0000,  65.4337]]),
 'l2': tensor([[ -9.3399,  48.2997,   3.9456, -14.9807,  -6.2132,   8.6506,   3.2942,
           -5.4517,  51.7242, -24.3171,  -4.3173, -15.2256,  14.6438, -39.9262,
           25.4930, -28.2587, -41.3537,  18.7739, -20.5236,   3.5374,   7.5814,
           24.1781, -26.5481, -20.8843,  19.6516,  32.6450, -35.9161,  36.6797,
           43.3538,  69.4207, -50.2650, -11.7544]]),
 'r2': tensor([[ 0.0000, 48.2997,  3.9456,  0.0000,  0.0000,  8.6506,  3.2942,  0.0000,
          51.7242,  0.0000,  0.0000,  0.0000, 14.6438,  0.0000, 25.4930,  0.0000,
           0.0000, 18.7739,  0.0000,  3.5374,  7.5814, 24.1781,  0.0000,  0.0000,
          19.6516, 32.6450,  0.0000, 36.6797, 43.3538, 69.4207,  0.0000,  0.0000]]),
 'l3': tensor([[18.9679,  6.6743, -2.2050]]),
 'sm1': tensor([[1.0000e+00, 4.5810e-06, 6.3786e-10]])}
```

After quantization:

```python
Layer: l1, Quantization Scale: 1.0, Zero Point: 0, Quantized Weights: tensor([[ -93, -123,   10,  -87],
        [  13,  -30,  -86,  -86],
        [  85,  124,  105,  -31],
        [ -92,   99,   -5,  -35],
        [  24,   69,  -92,    0],
        [ -48,  -56,   39,  -85],
        [ -57,  -34,   57,  -99],
        [ -22,  -80, -114,   62],
        [ -14,   30,  -53, -123],
        [ -78,   86,  -58,  -95],
        [ -15,   70,   71,   -5],
        [ -77,   14,   37,  -73],
        [  45,   72, -126,  127],
        [ -25,  -86,   47,  -96],
        [  93,   45,  -28,  -15],
        [ -37,  -65,   -1,  127],
        [  22,  109,  -29,   98],
        [  29,   53,   -9,   12],
        [-118,  -29,  -28,   28],
        [ 116,  -67,   95,  -39],
        [ -36,  -55, -125, -124],
        [ 111,  -99,   59,   25],
        [ -71,   60, -109,    1],
        [  27, -101,   23,  -45],
        [ 106,  -17,   75,   73],
        [  65,   24, -104,  -19],
        [  91,   90, -118,   14],
        [ 126,   -4,   24,  -90],
        [ -76,  -19,   24,  -83],
        [-125,   60,  -17,  -81],
        [-102,   94,   70,  -22],
        [  90,  -81,  111,  -32]], dtype=torch.int8), Quantized Biases: Parameter containing:
tensor([-0.2720,  0.3927, -0.4051,  0.0868,  0.4258,  0.0381,  0.4156,  0.4608,
         0.4082,  0.4499, -0.2309, -0.3837, -0.1326,  0.1527,  0.4145,  0.0167,
         0.4916,  0.3536, -0.4745,  0.0978, -0.0418, -0.2113, -0.0576, -0.4481,
         0.1094, -0.0918, -0.4603, -0.3162,  0.0935, -0.0163,  0.4653,  0.3049],
       requires_grad=True)
```

By performing tensor quantization, creating a integer matrix multiplier for forward propagation is much more efficient to compute within hardware. This way, the forward propagation can be completely in significantly less cycles with less area and at higher frequencies.

# Final Strategy Implementation

**Implemented Strategy:**

Upon initiating the project, the team set out to investigate the practicality of integrating moving averages and other technical indicators within a hardware-centric trading system. The foundational work involved performing basic arithmetic calculations through hardware, which established the groundwork for more intricate operations. The successful binary data parsing for the price_level_update protocol, as provided by IEX DEEP Market Data, was an early triumph that bolstered the team's resolve to push the boundaries of their trading strategy.

Advancing from this point, the team sought to harness the power of hardware to compute moving averages across a broader spectrum of data. The hardware's capability to swiftly calculate these averages for a multitude of data points presented an opportunity to refine the trading signals. Through rigorous software simulations, the team validated the hardware's outputs, ensuring accuracy and reliability before deployment.

The team's strategic approach was to calculate moving averages for specified data clusters, a decision that allowed for a focused and streamlined analysis of market trends without the added complexity of time stamp intricacies. This targeted approach aimed to extract the most salient features of market data for the generation of trading signals.

Further diversifying their strategy, the team incorporated two additional moving averages into their analysis: the 100-data-point moving average and the 1000-data-point moving average. These were meticulously computed from the rich dataset parsed from the market data and the SPY price_level_update protocol.

The logic underpinning the strategy remained consistent with the initial principle: when the current price surpasses these calculated moving averages, it triggers a buy signal, suggesting an upward momentum and a potential entry point for a long position. Conversely, if the current price dips below these averages, it signifies downward momentum, prompting a sell signal and indicating a potential short-selling opportunity.

By integrating these additional moving averages, the team aimed to capture a more granular view of market sentiment, allowing for the identification of both short-term fluctuations and more sustained market trends. The use of the 100-data-point and 1000-data-point moving averages intended to provide a spectrum of insights, from immediate market reactions to deeper, more established trends, thus offering a more intricate, complex and suspected more effective trading strategy.

The final goal was not only to exploit our hardware implementation and the market microstrcuture at a depper level, but also adding more complexity with moving averages that revealed patterns that are not noticed by other traders, possibily generating alpha.

**100 and 1000 Data Point Moving Averages:**

In contrast to the daily_moving_average previously implemented in the CSV file (as highlighted in Figure 2), obtaining the 100 and 1000 data point moving averages required some modifications. These two moving averages necessitated a minimum of 100 and 1000 price_level_update messages from IEX, meaning that no data would be available for the first 100 and 1000 price_level_updates of the day. Once a sufficient amount of data became available, the last 100 and 1000 price_level_update messages at the current moment were used to calculate these moving averages.

A parallel logic to that employed for the above_daily_moving_average and has_price_gone_up_yet columns, as previously detailed, was applied to these new 4 columns. In these columns, if the current price surpassed both the 100 and 1000 data point moving averages, the above_100DPA and above_1000DPA would transition to 1, and the hasPrice_gone_above_100DPA_yet and hasPrice_gone_above_1000DPA_yet would also become 1 at that point.

Even if the price continued to stay above both moving averages in subsequent messages, the has_price_gone_above_100DPA_yet and has_price_gone_above_1000DPA_yet would remain at 1, while the above_100DPA and above_1000DPA would revert to 0. These columns effectively served as buy signals, mirroring the functionality described in the earlier section (Parsing Code Analysis and Explanation) for the daily_moving_average.

**Implementing these Metrics into Existing CSV Files:**

As previously described, the team had already parsed several months' worth of IEX data pertaining to SPY, the Price_Level_Update protocols, and had generated CSV files for each day, including columns such as daily_moving_average, above_daily_moving_average, and has_price_gone_up_yet, which played a pivotal role in generating buy signals based on the original moving average strategy.

Rather than revisiting data parsing with a new script, which would have been extremely time-inefficient, the team adopted a different approach. The following steps were taken:

All the data from multiple months were consolidated into a single CSV file named "combined_market_data." This process involved merging the individual daily CSV files.
The 100-data-point moving average, above_100DPA (representing the buying signal), and has_price_gone_above_100DPA_yet columns were added to the combined CSV file.
The 1000-data-point moving average, above_1000DPA (representing the buying signal), and has_price_gone_above_1000DPA_yet columns were similarly included in the combined CSV file.
To achieve this, the team crafted the following scripts found in the projects git as Final Trading Stragey and Parsing.

```python
#combines all days of data
import pandas as pd
import glob
import os

def read_and_combine_csv(folder_path='.'):
file_pattern = os.path.join(folder_path, 'market_data_2023\*.csv')
csv_files = glob.glob(file_pattern)

    # Read and concatenate all CSV files
    combined_df = pd.concat([pd.read_csv(f) for f in csv_files])

    # Convert 'timestamp' column to datetime and sort
    combined_df['timestamp'] = pd.to_datetime(combined_df['timestamp'])
    combined_df.sort_values(by='timestamp', inplace=True)

    return combined_df

## Specify the folder path where your CSV files are stored

folder_path = '.' # Update this to the path of your CSV folder
combined_df = read_and_combine_csv(folder_path)

## Save the combined data to a new CSV file

combined_df.to_csv('combined_market_data.csv', index=False)

print("Combined CSV created: 'combined_market_data.csv'")`
```

**Description:**

This script begins by defining a file_pattern that specifies the naming convention for CSV files to be combined. It looks for files named 'market_data_2023\*.csv' in the specified folder.
Using the glob module, it identifies and compiles a list of all CSV files that match the specified naming pattern within the folder.
The script reads each of these CSV files and concatenates them into a single DataFrame called combined_df.
To ensure proper sorting, it converts the 'timestamp' column in combined_df to a datetime format, making it suitable for chronological ordering.
The DataFrame is then sorted based on the 'timestamp' column, arranging the data in chronological order from the earliest to the latest timestamps.
Finally, the sorted and combined data is saved to a new CSV file named 'combined_market_data.csv', with the index column excluded.
A message is printed to confirm the successful creation of the combined CSV file.

```python
#Creating The 100/1000 Data Point Average

import pandas as pd

## Load the CSV file

csv_file = "combined_market_data.csv"
df = pd.read_csv(csv_file)

## Calculate the rolling average for the last 100 rows and rename the column to '100DPA'

df['100DPA'] = df['price'].rolling(100, min_periods=100).mean()

## Initialize the new columns with zeros

df['above_100DPA'] = 0
df['has_price_gone_above_100DPA_yet'] = 0

## Track whether the price has crossed above 100DPA

price_above_100DPA = False

## Iterate through the DataFrame

for index, row in df.iterrows():
if row['price'] > row['100DPA']:
if not price_above_100DPA:
df.at[index, 'above_100DPA'] = 1
df.at[index, 'has_price_gone_above_100DPA_yet'] = 1
price_above_100DPA = True
else:
df.at[index, 'has_price_gone_above_100DPA_yet'] = 1
else:
price_above_100DPA = False

## Save the updated DataFrame back to the CSV file

df.to_csv(csv_file, index=False)`
```

**Description:**

This script begins by loading the CSV file named 'combined_market_data.csv,' which contains organized market data, including timestamps and price information.
It calculates the rolling average for the last 100 rows of price data using the rolling function. This moving average is then added to the DataFrame as a new column named '100DPA' (100-Data-Point Average).
Two new columns, 'above_100DPA' and 'has_price_gone_above_100DPA_yet,' are initialized with zeros. These columns will be used to track whether the price is currently above the 100DPA and whether it has crossed above it at least once.
A boolean variable, price_above_100DPA, is initialized as False to track whether the price is currently above the 100DPA.
The script iterates through the DataFrame row by row, examining each price data point and comparing it to the 100DPA.

When a row's price is above the 100DPA, the script checks if it's the first time in the current sequence (a sequence where the price remains above 100DPA). If it's the first time, it sets both 'above_100DPA' and 'has_price_gone_above_100DPA_yet' to 1, indicating that the price is currently above the 100DPA and has crossed it at least once.
If the price remains above 100DPA in subsequent rows within the same sequence, 'has_price_gone_above_100DPA_yet' remains set to 1.
If the price goes below the 100DPA, the price_above_100DPA flag is reset to False.
Finally, the updated DataFrame, now enriched with the 100DPA and tracking signals, is saved back to the original CSV file without including the index column. This allows for further analysis and visualization of the data.

---

```python
import pandas as pd

## Load the CSV file

csv_file = "combined_market_data.csv"
df = pd.read_csv(csv_file)

## Calculate the rolling average for the last 1000 rows and rename the column to '1000DPA'

df['1000DPA'] = df['price'].rolling(1000, min_periods=1000).mean()

## Initialize the new columns with zeros

df['above_1000DPA'] = 0
df['has_price_gone_above_1000DPA_yet'] = 0

## Track whether the price has crossed above 100DPA

price_above_1000DPA = False

## Iterate through the DataFrame

for index, row in df.iterrows():
if row['price'] > row['1000DPA']:
if not price_above_1000DPA:
df.at[index, 'above_1000DPA'] = 1
df.at[index, 'has_price_gone_above_1000DPA_yet'] = 1
price_above_1000DPA = True
else:
df.at[index, 'has_price_gone_above_1000DPA_yet'] = 1
else:
price_above_1000DPA = False

## Save the updated DataFrame back to the CSV file

df.to_csv(csv_file, index=False)`

```

**Description:**

This script begins by loading the CSV file named 'combined_market_data.csv,' which contains organized market data, including timestamps and price information.
It calculates the rolling average for the last 1000 rows of price data using the rolling function. This moving average is then added to the DataFrame as a new column named '1000DPA' (1000-Data-Point Average).
Two new columns, 'above_1000DPA' and 'has_price_gone_above_1000DPA_yet,' are initialized with zeros. These columns will be used to track whether the price is currently above the 1000DPA and whether it has crossed above it at least once.
A boolean variable, price_above_1000DPA, is initialized as False to track whether the price is currently above the 1000DPA.
The script iterates through the DataFrame row by row, examining each price data point and comparing it to the 1000DPA.
When a row's price is above the 1000DPA, the script checks if it's the first time in the current sequence (a sequence where the price remains above 1000DPA). If it's the first time, it sets both 'above_1000DPA' and 'has_price_gone_above_1000DPA_yet' to 1, indicating that the price is currently above the 1000DPA and has crossed it at least once.
If the price remains above 1000DPA in subsequent rows within the same sequence, 'has_price_gone_above_1000DPA_yet' remains set to 1.
If the price goes below the 1000DPA, the price_above_1000DPA flag is reset to False.
Finally, the updated DataFrame, now enriched with the 1000DPA and tracking signals, is saved back to the original CSV file without including the index column. This allows for further analysis and visualization of the data.

![Alt text](final_report_image_sources/Final_Parsing_of_Data_example.png)

Figure 4: Combined Market Data Generated CSV File with New columns

Please look at projects git Large_Scale_data_Parsin.zip for an example of the combined data csv file we generated and market data inital parsed data. This file will contain some of the market data we initially parsed using the scrip in November Data Collection and Final Trading Strategy and Data Parsing .

**Creating Strategy and Back Testing in Software:**

Once we generated the new combined CSV file, our team needed to devise a strategy for utilizing the newly identified buy signals or inflection points in our trading approach. Initially, our goal was to create a trading strategy that involved purchasing 10 securities of SPY whenever any of the buying signals for the daily moving average, 100 data point moving average, or 1000 data point moving average was triggered. Conversely, a selling signal was intended to be generated whenever the price fell below any of these three moving averages. However, our initial script, which operated on the combined CSV file (Figure 4), did not yield profitable results.

In our quest for a more profitable strategy, we conducted extensive experimentation with different combinations of buying and selling signals. Eventually, we found that the strategy that incurred the least amount of loss involved taking a position whenever all three moving averages were surpassed by the current price and selling when the price dropped below the 1000 data point moving average.

To implement this refined strategy, our team developed a script, which can be found in the project's Git repository Final Trading Strategy and Data Parsing. Additionally, the script was adapted to confirm that this combination of the three buy signals consistently yielded the most profitable outcomes, considering the various parameters we could adjust in the technical strategy's implementation. To expedite script execution, we limited our analysis to the data from the month of November, given the substantial volume of data.

```python
import pandas as pd
import matplotlib.pyplot as plt

def trading_strategy(file_path):
df = pd.read_csv(file_path)
df = df.dropna(subset=['price'])

    buy_positions = []
    sell_positions = []
    total_profit = 0
    order_number = 0
    transaction_details = []
    profit_database = []  # To store details for profit database

    for index, row in df.iterrows():
        buy_signals = [row['above_daily_moving_average'], row['above_100DPA'], row['above_1000DPA']]
        if sum(buy_signals) >= 3:
            buy_price = row['price']
            quantity = 10
            buy_positions.append((order_number, index, buy_price, quantity, buy_signals))
            order_number += 1

        for order, buy_index, buy_price, quantity, buy_signals in list(buy_positions):
            current_price = row['price']
            profit_percentage = ((current_price - buy_price) / buy_price) * 100

            if profit_percentage > 6 or (current_price <= row['1000DPA']):
                if (order, buy_index, buy_price, quantity, buy_signals) in buy_positions:
                    profit = (current_price - buy_price) * quantity
                    total_profit += profit
                    sell_positions.append((order, index, current_price, quantity, profit))
                    transaction_details.append((order, (buy_index, buy_price, buy_signals), (index, current_price), profit))
                    profit_database.append({'Order': order, 'Buy Index': buy_index, 'Sell Index': index, 'Profit': profit, 'Positive Profit': profit > 0, 'Buy Signals': buy_signals})
                    buy_positions.remove((order, buy_index, buy_price, quantity, buy_signals))

    # Mark unsold transactions
    unsold_transactions = [{'Order': pos[0], 'Index': pos[1], 'Buy Price': pos[2], 'Quantity': pos[3], 'Buy Signals': pos[4]} for pos in buy_positions]

    return sell_positions, total_profit, transaction_details, profit_database, unsold_transactions

## Define the file path

file_path = 'combined_market_data.csv'

## Execute the strategy

sell_positions, total_profit, transaction_details, profit_database, unsold_transactions = trading_strategy(file_path)

## Output the results

print("\nUnsold Transactions:")
for unsold in unsold_transactions:
print(unsold)

print("\nTotal Profit:", total_profit)

## Create DataFrame for profit database

profit_df = pd.DataFrame(profit_database)
print("\nProfit Database:")
print(profit_df)
number_of_orders = profit_df['Order'].nunique()
print("Number of Orders:", number_of_orders)

## Plotting the profit timeline

profits = [td[3] for td in transaction_details] # Extract profits from transaction details
cumulative_profits = [sum(profits[:i+1]) for i in range(len(profits))] # Cumulative sum of profits

plt.figure(figsize=(10, 6))
plt.plot(cumulative_profits, marker='o', linestyle='-')
plt.title('Cumulative Profit over Transactions')
plt.xlabel('Transaction Number')
plt.ylabel('Cumulative Profit')
plt.grid(True)
plt.show()

## Filter the DataFrame to show only profitable transactions

profitable_transactions = profit_df[profit_df['Positive Profit'] == True]

## Print profitable transactions

print("\nProfitable Transactions:")
print(profitable_transactions)
```

**Script Description and Functionality: Trading Strategy Implementation**

This script, responsible for implementing our trading strategy, plays a pivotal role in tracking and managing buy and sell positions, monitoring profits, and accumulating data for later backtesting. Its primary objective is to translate the predefined strategy into automated actions and record transaction details for thorough analysis.

Data Loading and Initialization:
The script begins by importing essential libraries, including Pandas for data manipulation and Matplotlib for visualizations.
It defines a function called trading_strategy that takes a file path as input to process the market data.
Upon reading the specified CSV file (containing combined market data), the script creates a Pandas DataFrame, dropping rows with missing price data to ensure data integrity.
Position Tracking and Profit Calculation:
The script initializes several lists and variables to keep track of various aspects throughout the trading process:
buy_positions: Stores information about open buy positions, including order number, buy index, buy price, quantity, and buy signals.
sell_positions: Records details of successful sell transactions, including order number, sell index, current price, quantity, and profit.
total_profit: Accumulates the total profit generated by the trading strategy.
order_number: Assigns a unique order number to each buy position, facilitating order identification.
transaction_details: Maintains a record of each transaction's details, including order number, buy and sell indices, profit, and buy signals.
profit_database: Stores comprehensive information about each transaction, including order number, buy and sell indices, profit, and buy signals.

**Buy Signal Evaluation:**
The script iterates through each row of the market data DataFrame, evaluating the buy signals generated by the strategy. These buy signals include:
above_daily_moving_average: Indicates whether the price is above the daily moving average.
above_100DPA: Signals if the price is above the 100 data point moving average.
above_1000DPA: Detects if the price is above the 1000 data point moving average.
If all three buy signals are met (sum of buy_signals >= 3), a buy position is established.
Information about the buy order, such as order number, index, buy price, quantity, and buy signals, is recorded in the buy_positions list.

**Profit Monitoring and Sell Conditions:**
The script continuously monitors existing buy positions. For each open buy position:
It calculates the current price and profit percentage compared to the buy price.
It checks if any of the predefined sell conditions are met:
If he current price falls below the 1000 data point moving average, a sell condition is triggered.
If a sell condition is met:
The script calculates the profit, updates the total_profit, and records the details of the sell transaction in the sell_positions, transaction_details, and profit_database lists.
The buy position is removed from the buy_positions list to indicate that it has been sold.

**Results and Output:**

The script provides several outputs for analysis:
Unsold Transactions: Lists information about any unsold buy positions, including order number, index, buy price, quantity, and buy signals.
Total Profit: Displays the cumulative profit generated by the trading strategy.
Profit Database: Creates a Pandas DataFrame (profit_df) containing comprehensive transaction details, including order number, buy and sell indices, profit, and buy signals.
Number of Orders: Calculates and displays the total number of orders executed during the trading period.

**Visualization:**
The script generates a visual representation of the cumulative profit over transactions using Matplotlib. This plot provides insight into the overall performance of the trading strategy.
Filtering Profitable Transactions:
The script filters the profit_df DataFrame to isolate and display only profitable transactions. These transactions are characterized by a positive profit.

# Backtesting

The trading strategy was designed to evaluate the profitability of a specific set of buy signals based on moving averages. The code for the trading strategy is provided, and we will explain how it was used for backtesting. The backtesting was performed on combined CSV files for the months of November, October, September, and August of 2023, totaling approximately 5,100,000 data points. These individual monthly datasets were combined into a single CSV file named 'combined_market_data.csv'. This consolidation allowed for a comprehensive analysis of the strategy's performance over a more extended period.

The subsequent section provides a detailed breakdown of how the backtesting code executed, highlighting the crucial elements:

1. Data Loading and Cleansing
   Data Import: The process commenced by importing the historical market data from 'combined_market_data.csv' into a Pandas DataFrame. This DataFrame served as the foundation for the analysis.
   Data Cleansing: To maintain data integrity, the code performed a critical cleansing step. Rows with missing 'price' values were systematically removed from the dataset, ensuring that only complete and valid data points were considered.
2. Initialization
   Variable Initialization: Key variables were initialized to capture and manage essential information throughout the backtesting procedure. These included:
   buy_positions: A list to track buy positions.
   sell_positions: A list to track sell positions.
   total_profit: A variable to accumulate the total profit or loss.
   order_number: A variable to uniquely identify each transaction.
   transaction_details: A list to store comprehensive transaction information.
   profit_database: A structured database to record results.
3. Iterating Through Data
   Systematic Data Iteration: The core of the backtesting code revolved around systematically iterating through each row of the DataFrame representing historical market data.
   Buy Signal Evaluation: During each iteration, the code meticulously assessed the buy signals by examining conditions such as above_daily_moving_average, above_100DPA, and above_1000DPA. It also tracked the cumulative count of buy signals.
   Buy Position Creation: When the cumulative count of buy signals reached or exceeded 3, a buy position was initiated. This buy position captured crucial details, including order number, index, buy price, quantity, and buy signals.
4. Monitoring and Selling Positions
   Real-time Monitoring: Continuous tracking of the current asset price was incorporated into the code. As the market data was processed, the code kept a close watch on price movements.
   Profit Threshold and Selling: The code implemented profit-taking criteria, signaling a position for sale when either of the following conditions was met:
   The profit percentage exceeded 6%.
   The current price dropped below the 1000-day moving average (1000DPA).
   Profit Calculation: Upon a sale signal, the code calculated the profit or loss from the transaction and logged it accordingly.
   Buy Position Removal: Simultaneously, the code removed the corresponding buy position from the buy_positions list, ensuring accurate tracking of active positions.
5. Recording Transaction Details
   Comprehensive Documentation: Every transaction, whether a buy or sell, was meticulously documented in the transaction_details list. This documentation encompassed essential information, including order number, buy and sell indices, profit, and buy signals.

6. Results Presentation
   Unsold Transactions: This section of the code provided detailed insights into any buy positions that were not successfully sold during the backtesting process.
   Total Profit: The cumulative profit or loss generated by the trading strategy was reported, providing a clear picture of the strategy's financial performance.
   Profit Database: A structured Pandas DataFrame was constructed, offering an organized view of transaction details, including order numbers, buy and sell indices, profits, and buy signals.
   Number of Orders: The code calculated the total number of orders executed, serving as a fundamental metric in assessing the strategy's trading activity.
   Profit Plotting: To convey the strategy's performance visually, the code employed Matplotlib to generate a graphical representation of cumulative profits over the sequence of transactions.

**Summary of Backtesting and Limitations in Methods**

The team's refined trading strategy was predicated on a multi-layered moving average crossover system. The backtesting logic was programmed to execute trades when the SPY ETF's current price, as indicated by the IEX price_level_update, surpassed the daily moving average (DMA), the 100-data-point moving average (100DPA), and the 1000-data-point moving average (1000DPA). In such an event, the algorithm was set to automatically purchase 10 units of SPY, assuming that these positions could be acquired at the current market price specified in the IEX data.

Position Liquidation Protocol:
For the sale of positions, the strategy employed a first-in, first-out (FIFO) methodology. Each buy order was recorded with a unique identifier, and the corresponding sell order was executed when the current price either fell below or matched the 1000DPA. It was assumed that these positions were liquidated at the current price detailed by the price_level_update protocol, capturing the prevailing market conditions at the time of the transaction.

Backtesting Limitations and Assumptions:
The backtesting environment was developed using Python due to its capability to handle large datasets and perform complex calculations with agility. This approach allowed the team to analyze approximately 5.1 million data points from combined CSV files over several months. The backtesting simulation assumed that all transactions were cleared at the stated current prices without factoring in transaction costs, which are a significant consideration in real-world trading scenarios.

The team was aware that the absence of transaction cost considerations could lead to an overestimation of the strategy's profitability. Additionally the team was aware that trades are not done immediately at the current price level stated in the CSV file due to the depth of book considerations, in real life trading the depth of book is a huge factor that has to be considered to understand at what price the poisition is able to be taken a liquidated

Despite these constraints, the team was confident in the Python script's efficacy for the backtesting purposes given the vast volume of data and number of transactions processed. The belief was that the sheer scale of the data would offset some of the inaccuracies most speficically the transaction costs. Most importantly the main goal of the project was translating our strategy to hardware, our current hardware implementation will have latencies which are extremely competitive and probably even faster than any other software based implementation. The purpose of the python backtesting was to further develop on the strategy and make changes in software that can be then translated into hardware in the future. Additionally, please note that Strategy Studio which could also be used for backtesting doesn't have any interface for Vivado software for FPGA's.

The outcome of the backtesting process unequivocally demonstrated that the trading strategy, as implemented, did not yield a profit. Despite the meticulous implementation of buy signals and profit-taking criteria, the overall result was a net loss, substantiated by the "Total Profit" value.

**Conclusion**

In conclusion, While the backtesting endeavor did not achieve profitability, the primary objective was not financial gain. Instead, it aimed to showcase the systematic and methodical approach involved in assessing trading strategies using historical data.

The thorough backtesting process encompassed critical steps, including data consolidation, strategy implementation, real-time position monitoring, and comprehensive result analysis. This exercise serves as an invaluable reference for understanding the intricacies of backtesting and lays a solid foundation for further research and development of trading strategies.

![Alt text](final_report_image_sources/Final_Profits.png)

**Figure 5: Backtesting Results with over 5,000,000 price_level_updates**

# Other Strategies Researched: Machine Learning Development

**Overview:**

We try to build a neural network model built for predicting stock trading actions, with a focus on its architecture and layout. The model aims to analyze historical stock data to inform trading decisions.

**Model Architecture and Layout:**

- Type and Structure: The model is a Recurrent Neural Network (RNN) with Long Short-Term Memory (LSTM) layers, optimized for sequential data processing. The LSTM layers are crucial for capturing temporal dependencies in stock price movements. The model uses LSTM layers because of their ability to capture long-term dependencies in sequential data, a critical feature in financial time series where past trends can significantly influence future prices.

- Sequential Input Layer: Receives sequential data including features such as stock prices, daily moving averages, and binary indicators like 'above_daily_moving_average' and 'has_price_gone_up_yet' from IEX data we have processed already, reflecting the temporal nature of the stock market where the sequence and timing of price changes are pivotal for predicting future movements.

- Hidden Layers with ReLU Activation: Multiple LSTM layers form the core of the model, processing the temporal sequence of inputs. Additional dense layers (fully connected layers) with ReLU activation functions are used for nonlinear transformations of the data. ReLU activation functions in dense layers are chosen for their efficiency and effectiveness in mitigating the vanishing gradient problem, common in deep neural networks.

- Output Layer: A final dense layer with a softmax function outputs the probabilities of each trading action (‘Buy’ and ‘Sell’). The softmax output layer is utilized to provide a probabilistic output across the trading actions. This approach is beneficial for decision-making processes that require an understanding of relative confidence across possible actions.

**Data Flow and Processing:**

- Multi-Source Data Collection: Data is aggregated from multiple CSV files, ensuring a comprehensive dataset that captures diverse market scenarios.
  Preprocessing: The data undergoes preprocessing where sequences of data points are created, suitable for RNN input. NaN values are handled, and data normalization is applied as needed to ensure consistency in feature scales. Aggregating data from multiple sources ensures a rich and diverse dataset, capturing various market conditions and trends, which is essential for building a robust model.

- Feature Engineering: Key features for the model include price-related metrics and trend indicators, selected based on their relevance to stock market dynamics.

- Preprocessing Strategy: Preprocessing, including normalization and handling of missing values, is critical to maintain consistency in the model's input, ensuring that the network learns from relevant patterns in the data.

**Hardware Implementation**

In order to implement such a module in hardware, many matrix multiplication units would be required to perform all of the relevant computation. Xilinx provides multiplicaiton cores that can do multiplication on whole numbers, however, to support a fully intact ML model we would need to use fractional values for our weights. Mentioned above is the impementation of the IEX DEEP number representation to IEEE754. Another matrix multiplication would have to be developed to handle this type of number. This module would likely require many cycles to run, as the critical path through many of these multiply units would be extremely large. The blackbox ability of the algorithm module as well as the AXI bus would allow such a strategy to run on our device.

**Training Process and Strategy:**

- Profitability-Focused Labeling Strategy: Labels (‘Buy’ and ‘Sell’ signals) are generated based on a predefined strategy that aligns with historical profitability, which could be derived from historical patterns indicating profitable trading opportunities. This approach is intended to ensure that the model's predictions are not just theoretically sound but also practically viable in terms of generating profitable outcomes.

Training Mechanism: The model is trained using backpropagation through time (BPTT), a variant of gradient descent suitable for RNNs. Cross-Entropy Loss is used to measure the discrepancy between the model's predictions and actual labels. Backpropagation through time is specifically suited for training RNNs as it allows the model to learn from errors at each step in the sequence, a necessary feature for time series data like stock prices.

Batch Processing: Data is fed into the model in batches, enhancing computational efficiency and enabling more stable gradient updates.
Strategy Design and Integration

Decision Support Framework: The model is a component of a broader decision-support framework, providing predictive insights to guide trading decisions.

Iterative Refinement: Continuous model refinement is emphasized, with adjustments based on backtesting results and market evolution.

**Evaluation and Enhancements:**

- Backtesting and Performance Metrics: The model's predictions are rigorously tested using historical data to simulate trading performance. Accuracy, precision, recall, and profitability metrics are employed to evaluate the model’s effectiveness. Rigorous backtesting and a focus on key performance metrics are critical for evaluating the model’s real-world applicability and ensuring that it can adapt to new data and market conditions.

Future Directions: Potential enhancements include incorporating a wider range of market data, exploring advanced neural network architectures, and developing mechanisms for real-time market adaptation. Exploring advanced neural network architectures and expanding data sources are driven by the need to capture more complex market dynamics and improve the model’s predictive power.

**Conclusion:**

The designed ML algorithm, with its focus on LSTM structures and sequential data processing, represents a targeted approach to harnessing historical stock data for predictive analysis in trading, which is grounded in the unique characteristics of financial time series data and the specific requirements of stock trading. Its effectiveness is rooted in the careful design of its architecture and the integration of its output within a comprehensive trading strategy. Ongoing evaluation, adaptation, and enhancement are key to maintaining its relevance in dynamic market conditions. The model’s architecture, data handling, training strategy, and integration into a broader decision-making framework are all tailored to address the complexities of predicting stock market movements. Continuous evaluation, adaptation, and potential enhancements are part of the model’s lifecycle, ensuring its relevance and effectiveness in the ever-changing world of stock trading.

# Project Reflection

## Adrian Cheng

**What did you specifically do individually for this project?**

- Python PCAP Parser to convert PCAP files into verilog readable memory hex dumps
  - Converted the binary into JSON
  - Used JSON's raw HEX dump to parse out in little endian order for verilog `$readmemh`
- Fully parameterizable testbench harness
  - Ability to change the following parameters dynamically
    - Simulation starting packet
    - Simulation ending packet
    - Maximum simulation length
    - Timeskipping
    - Progress logging
    - Progress logging denomination
  - Timeskipping
    - Allow for packets to be replayed in real-time with nanosecond accuracy when timeskipping is disabled
    - Allow for packets to be delivered immediately after the algorithm is ready to allow for more extensive backtesting
- Native PCAP parsing within testbench harness
  - Eliminated the need for an external Python script to parse out PCAP files
  - Streamed PCAP files dynamically during simulation to avoid reading the entire PCAP file at once
    - Significant speed improvement in file read speeds compared to using a memory dump file
  - Supports all parameterizable features stated above
- Hardware implementation of the momentum strategy
  - Simple 4 state state machine to manage whether the current price has pushed above or below the moving averages
  - Dedicated DUT testbench for the verification of the strategy functional in hardware
- IEEE 754 32-bit Floating Point Price Conversion
  - Converts the proprietary fixed decimal format the IEX provides to the standardized floating point
    format
- ML Quantization and Hardware Implementation Research
  - Explored the different quantization parameters available for trained fully connected neural networks for hardware implementation
  - Prototyped the use of systolic arrays for fast hardware matrix multiplication for a significant speedup during forward propagation
  - This idea was ultimately abandonded due to a shift in project direction
- Alveo x3522 PV Firmware Flashing
  - Prepared and attempted to flash the Alveo x3522 PV firmware
    - Was not able to successfully able to flash due to incorrect firmware version
    - Attempted to update x3522 firmware
      - Unsuccessful due to kernel not detecting the pci device when running `devlink dev info pci/0000:05:00.0` despite showing up under `lspci`
  - Installed all the required flashing drivers as well as the rpm bundle for flashing
  - Decided to allocate more time on actual testbench harness development rather than to continue troubleshooting

**What did you learn as a result of doing your project?**

- Intracacies of how the PCAP file format works
- Advanced System Verilog constructs that allow dynamic data reading and typecasting
- Managing extremely large data sources and processing them efficiently
- Neural Net Quantization
- Systolic arrays and it's implementation
- IEEE floating point bit representation conversion
- IEX DEEP protocol formatting and parsing in binary
- IEX TP Packet headers
- Alveo x3522 Board setup in Vivado
- IP integration within Vivado
- Creating dynamic and parameterizable DUT testbenches

**If you had a time machine and could go back to the beginning, what would you have done differently?**

- Setup strategy studio and run a simple fully connected ML network against it to start generating weights before hardware implementation
- Create more hardware implemntations of different strategies to see differences in performance
- Start board bring up process earlier, allowing for more time to troubleshoot
- Read the IEX DEEP protocol documentation more carefully, would save a lot of time if not for debugging several misinterpretations
- Install wireshark IEX plugin earlier, saves a lot of time trying to understand raw hex dumps manually

**If you were to continue working on this project, what would you continue to do to improve it, how, and why?**

Given the chance to continue this project, I would definitely want to continue pursuing the path of ML and neural net implementation within hardware. I would most likely focus more on training a basic fully connected neural net against the profitability generated through strategy studio first. After weights have been sufficiently trained, I will then quantize the weights within the trained network and port it over to a memory dump file so that my hardware systolic array can perform forward propagation using the weights genereted by the software training. Given even more time, I would research into performing back propagation as well as gradient descent within hardware, allowing the training process to learn at a significantly increased rate using dedicated hardware.

**What advice do you offer to future students taking this course and working on their semester long project (be sides “start earlier”… everyone ALWAYS says that). Providing detailed thoughtful advice to future students will be weighed heavily in evaluating your responses**

My main advice for future students, specifically in attempting hardware based projects like these is that it will always take longer than what you expect to do certain things. Even with my prior experience in hardware development, it still took me by surprise on how long the actual state machine was to implement for the momentum algorithm. Hardware bugs are sometimes incredibly hard to find, so it is crucial to setup a good testing environment with dedicated unit testing testbenches for each of your components so that it is much easier to catch bugs. Another note is that just because everything works individually, doesn't mean everything will work together. Hardware integration is shockingly difficult, with very strange interaction bugs only popping up when you try to hook up two or more modules together, make sure to allot plenty of time for integration testing as that will probably take even longer than getting the individual modules to function.


## Jason Romps

**What did you specifically do individually for this project?**
- Developed harness between testbench, interface, and algorithm
  - AXI between interface and algorithm
- Developed state machine for interface
  - 7 states total
  - Checked for livelock/deadlocks between states
  - Blackboxed modules whenever possible to allow for future development
- Created extensive testbenches for transferring data between all modules
  - Different testbenches test different amount of SPY packets
  - Cover edge cases with broken packets
  - Testcase with massive sample sizes
- Wireshark data capture and translation from pcap -> json -> mem
- Integration between testbench and interface allowing for timeskipping
- Vivado Port
  - Ported our hdl to Vivado and set up Vivado interface
  - Simluation setup in Vivado
  - Setup board and timing constraints
- Momentum strategy
  - Created initial framwork for momentum strategy
  - Dynamic FIFOs for storing most recent data
  
  


**What did you learn as a result of doing your project?**
- All sorts of SystemVerilog features
  - PCAP parsing
  - Reading mem files
- When should specific modules be blackboxed and creating a proper integration method between modules
- IEEE754 and how it makes multiplication easier
- How Vivado reads board files and sdf files
- Ethernet / UDP protocols
- IP Generator in Vivado and grabbing its source code
  



**If you had a time machine and could go back to the beginning, what would you have done differently?**

- Spend more time testing strategy before putting it in hardware. 
- Reach out to ECE professors to help us with specific verilog bugs that we spent lots of time debugging. 
- Create a more intensive make file that allows for parameters
  - Allows us to test more efficiently 
- Keep a tighter view on the software and hardware side of the project
  - Most of my time was spent developing in HDL

**If you were to continue working on this project, what would you continue to do to improve it, how, and why?**

I would continue to improve it by developing an entire suite of algorithms that can be plugged into the interface. The plug and play ability makes it super easy to try new algorithms. Some things I would want to try is with multiple symbols, matrix multiplication for ML, and looking for other protocols besides buy/sell. I would also want to develop creating the actual Ethernet packet that says I would like to buy. Creating the tickerplant that sends the UDP to our harness could then accept this buy order and would then spit it back to us.

**What advice do you offer to future students taking this course and working on their semester long project (be sides “start earlier”… everyone ALWAYS says that). Providing detailed thoughtful advice to future students will be weighed heavily in evaluating your responses**

My main advice is to really have a solid end goal. Having a solid end goal for not only the project but for each person is super useful in tracking progress. Otherwise it gets too hard with understanding goals of each person and their individual progress. For team leaders specifically, make sure that the goals you give each person are distinct and obtainable. Group members that have open ended goals will less motivated to complete them and harder for you to check it off. For hardware projects specifically have a plan. Do you want to target a specific board? Do you want to have a in-depth testbench? Set these goals out firm at the very beginning. Also for team leaders, have more than 1 meeting a week. Being able to have group discussions about the project progress is massive and keeps everyone in the loop and motivated.



## Lijiajin Lin

**What did you specifically do individually for this project?**

- Assisting in Model Design: I helped conceptualize the model architecture, including the selection of appropriate layers, activation functions, and the structure of the neural network suitable for time-series analysis.

- Data Handling Strategies: I advised on strategies for data preprocessing, feature selection, and handling missing values or anomalies in the stock market data, which are critical for the model's performance.

- Training and Evaluation Methods: I provided guidance on the model training process, including the selection of loss functions, optimization techniques, and strategies for model evaluation like backtesting.

- Interpreting Financial Time Series Data: I contributed insights into the complexities of financial data, emphasizing the importance of understanding temporal dependencies and market dynamics for effective modeling.

- Iterative Improvement and Adaptability: I emphasized the need for continuous evaluation and adaptation of the model, considering the evolving nature of financial markets.

**What did you learn as a result of doing your project?**

- Importance of Data Quality and Preprocessing: The project underscored how crucial high-quality, relevant data is for the success of any ML model. Proper preprocessing, including handling missing values, normalization, and feature selection, significantly impacts the model's ability to learn meaningful patterns.

- Complexity of Financial Time Series Data: Working with stock market data reinforced the understanding of its complex, noisy, and non-stationary nature. It highlighted the necessity of using models like RNNs with LSTM layers that are adept at capturing temporal dependencies and long-term trends in time-series data.

- Balance Between Model Complexity and Interpretability: The project demonstrated the delicate balance between creating a model complex enough to capture market dynamics and simple enough to maintain interpretability and avoid overfitting.

- Adaptability to Market Changes: The project highlighted the dynamic nature of financial markets and the need for models to adapt. It showed the importance of regularly updating the model with new data and potentially adjusting its architecture to respond to market changes.


**If you had a time machine and could go back to the beginning, what would you have done differently?**
I will start early for backtesting part and train model earlier to get weights so hardware part can be much smooth.


**If you were to continue working on this project, what would you continue to do to improve it, how, and why?**
- Enhance Data Quality and Variety: The reason is that our data is too big and it takes long time to train the model. Incorporate additional data sources such as macroeconomic indicators, company fundamentals, and market sentiment analysis from news and social media. Implement advanced techniques for anomaly detection and data normalization.

- Refine the Model Architecture: Experiment with different neural network architectures, including deeper LSTM layers, attention mechanisms, or convolutional layers for feature extraction. Implement dropout and regularization techniques to prevent overfitting.

- Implement Advanced Feature Engineering: Develop more sophisticated features using techniques like Principal Component Analysis (PCA) or t-Distributed Stochastic Neighbor Embedding (t-SNE) for dimensionality reduction. Explore technical indicators used in quantitative finance.


**What advice do you offer to future students taking this course and working on their semester long project (be sides “start earlier”… everyone ALWAYS says that). Providing detailed thoughtful advice to future students will be weighed heavily in evaluating your responses**

I think it is relative hard on connecting ML model with SS by using C++. You need to set up SS earlier and prepare a model that's fitable for SS early. It can be much harder than you thought.
