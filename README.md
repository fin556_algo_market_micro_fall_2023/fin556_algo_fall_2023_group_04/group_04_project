# group_04_project

# Meet The Team:

## Jason Romps
I am a Senior at UIUC studying Computer Engineering with a concentration in digital design and will be graduating in May 2024. I have lots of industry experience in FPGA & ASIC design and verification. I am the team lead for our low-latency hardware trading application. You can reach me at jasonar2@illinois.edu


## Diego Rapppaccioli
I am a Senior at UIUC studying Industrial Engineering with a minor in Computer Engineering and will be graduating in December. I have basic programming experience and have been able to work with large financial firms in the past. You can reach me at diegor3@illinois.edu. My Linkedin: is https://www.linkedin.com/in/diego-rappaccioli-gomez-10207b1b4


## Lijiajin Lin
I am a Senior at UIUC studying Computer Science concentrated in system design and will be graduating in May 2024. I have lots of  experience in distributed system design. You can reach me at llin17@illinois.edu

## Adrian Cheng
I am a Senior at UIUC studying Computer Engineering graduating in December 2023. I have industry experience in full stack development and I have experience in created automated deployments in a variety of environments. You can reach me at acheng27@illinois.edu.
